// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDsvKOsCADd4YsvXS5c4kXU_MahcZl7X7w",
    authDomain: "seven-retail-dev.firebaseapp.com",
    projectId: "seven-retail-dev",
    storageBucket: "seven-retail-dev.appspot.com",
    messagingSenderId: "644358109383",
    appId: "1:644358109383:web:66bdc85a504c1e4851e6e2",
    measurementId: "G-50SW0TTH3W"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const database = getFirestore(app);
export const storage = getStorage(app);