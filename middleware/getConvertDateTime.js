var date = new Date()
var options = { hour12: false };
const getConvertDateTime = (datetime) => {
    return datetime.toLocaleString("id-ID", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    }).substr(0, 10) + " " + datetime.toLocaleTimeString('id-ID', options).substr(0, 5)
}

export default getConvertDateTime;