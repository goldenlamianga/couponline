var date = new Date()
var options = { hour12: false };
const getConvertDate = (datetime) => {
    return datetime.toLocaleString("id-ID", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    }).substr(0, 10)
}

export default getConvertDate;