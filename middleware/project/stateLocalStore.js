const stateLocalStore = (handler) => {
    console.log("stateLocalStore parse " + handler);
    const data = {
        "provinsi": "",
        "kota": "",
        "kec": ""
    }
    try {
        // data = {
        //     "provinsi": handler.prov,
        //     "kota": handler.kota,
        //     "kec": handler.kec
        // }
        // console.log("prov " + data.prov);
        // console.log("kota " + data.kota);
        // console.log("kec " + data.kec);
        localStorage.setItem('state', JSON.stringify(handler));
        // localStorage.setItem('isLoggedIn', true);
        return true
    } catch (error) {
        console.log(error);
        localStorage.setItem('isLoggedIn', false);
        return null;
    }

}

export default stateLocalStore;