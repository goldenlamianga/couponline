const projectStore = (handler) => {
    const data = {
        "nama": "",
        "address": "",
        "title": "",
        "img": ""
    }
    try {
        data = JSON.stringify(handler)
        localStorage.setItem('project', JSON.stringify(handler));
        return data
    } catch (error) {
        console.log(error);
        return null;
    }

}

export default projectStore;