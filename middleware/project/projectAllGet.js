const projectAllGet = () => {
    try {
        const data = JSON.parse(localStorage.getItem('projectAll'))
        return data;
    } catch (error) {
        console.log(error);
        return null;
    }

}

export default projectAllGet;