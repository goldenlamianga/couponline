const projectAllStore = (handler) => {
    const data = [{
        "nama": "",
        "title": "",
        "address": "",
        "img": ""
    }]
    try {
        data = JSON.stringify(handler)
        localStorage.setItem('projectAll', JSON.stringify(handler));
        return data
    } catch (error) {
        console.log(error);
        return null;
    }

}

export default projectAllStore;