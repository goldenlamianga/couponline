const projectGet = () => {
    try {
        const data = JSON.parse(localStorage.getItem('project'))
        return data;
    } catch (error) {
        console.log(error);
        return null;
    }

}

export default projectGet;