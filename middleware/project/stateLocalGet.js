const stateLocalGet = () => {
    try {
        const data = JSON.parse(localStorage.getItem('state'))
        return data;
    } catch (error) {
        console.log(error);
        return null;
    }

}

export default stateLocalGet;