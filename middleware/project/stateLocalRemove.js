const stateLocalRemove = (handler) => {
    try {
        localStorage.removeItem("state");
        return true
    } catch (error) {
        console.log(error);
        localStorage.setItem('isLoggedIn', false);
        return null;
    }

}

export default stateLocalRemove;