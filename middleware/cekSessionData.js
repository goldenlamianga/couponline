import userLocalRemove from "./user/userLocalRemove";
import { hash, compare } from 'bcryptjs'
import Router from "next/router";
import resetDataCollection from "./resetDataCollection";
import encryptText from "./encdecrypt/encryptText";
import decryptText from "./encdecrypt/decryptText";

const SESSION_TIME = 1 * 50 * 1000;
const cekSessionData = async () => {
    const now = Date.parse(new Date());
    if (localStorage.getItem("sessionDataCollection") == null) {
        //RESET DATA PROJECT COLLECTION
        localStorage.setItem("sessionDataCollection", await encryptText(now.toString()))
    } else {
        var dt = await decryptText(localStorage.getItem("sessionDataCollection"))
        // var dt = localStorage.getItem("sessionDataCollection")
        var compare = now - dt;
        console.log("SESSION_TIME : " + SESSION_TIME)
        console.log("sessionDataCollection : " + await decryptText(localStorage.getItem("sessionDataCollection")))
        console.log("dt : " + dt)
        console.log("now : " + now)
        console.log("compare : " + compare)
        if (compare > SESSION_TIME) {
            //RESET DATA PROJECT COLLECTION
            console.log("reset new session : " + now)
            localStorage.setItem("sessionDataCollection", await encryptText(now.toString()))
        }
        //DATA PROJECT COLLECTION MASIH ADA
    }


    // const d = new Date();
    // d.setDate(d.getDate() - 1);


    return now

    const date = new Date();
    // date.setDate(date.getDate() - 1)
    date.setDate(date.getDate());
    const dtNow = date.toLocaleDateString()
    console.log(dtNow)
    const sessionTime = await localStorage.getItem("sessionTime")
    console.log("==========cekSession===========>" + sessionTime)
    if (!sessionTime) {
        resetDataCollection().then(() => {
            hash(dtNow.toString(), 0).then((rst) => {
                localStorage.setItem("sessionTime", rst)
            })
            console.log("Create New SessionTime")
        })
    } else {
        console.log("===========Compare===========>")
        // console.log(dtNow.toString() + " <=> " + localStorage.getItem("sessionTime"))
        const match = await compare(dtNow.toString(), localStorage.getItem("sessionTime"));
        console.log("=========cekSession==========>" + match)
        if (!match) {
            console.log("Reset All Session")
            resetDataCollection().then(() => {
                hash(dtNow.toString(), 0).then((rst) => {
                    localStorage.setItem("sessionTime", rst)
                })
                console.log("Start New SessionTime")
                console.log("Direct to onboard")
                return Router.push('/onboard')
            })
        }
    }
}

export default cekSessionData;