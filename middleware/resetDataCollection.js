const resetDataCollection = async (handler) => {
    try {
        //Login User
        localStorage.setItem('isLoggedIn', false)

        localStorage.removeItem('sessionTime')

        // profile Collection 
        localStorage.removeItem('userDataCollection')

        // Master Menu Collection
        localStorage.removeItem("menuLocalParam")
        localStorage.removeItem("menuLocalByCategory")

        //Master Project Collection
        localStorage.removeItem("projectAll")
        localStorage.removeItem("project")

        // Master State Provinsi,Kota & Kec 
        localStorage.removeItem("state")

        // Chart, Payment & Transaksi Collection
        localStorage.removeItem('userLocalParamTrans')
        localStorage.removeItem('selectedMethod')
        localStorage.removeItem('hasCart')
        localStorage.removeItem('itemCart')
        return true
    } catch (error) {
        console.log(error)
        return null;
    }

}

export default resetDataCollection;