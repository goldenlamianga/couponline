import { hash } from 'bcryptjs'
const setSession = async () => {
    const date = new Date();
    date.setDate(date.getDate());
    const dtNow = date.toLocaleDateString()
    console.log("Create sessionTime : " + dtNow)
    hash(dtNow.toString(), 0).then((rst) => {
        localStorage.setItem("sessionTime", rst)
    })
}

export default setSession;