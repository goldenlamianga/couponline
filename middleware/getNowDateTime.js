var date = new Date()
var options = { hour12: false };
const getNowDateTime = () => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var todays = mm + '/' + dd + '/' + yyyy;

    var times = today.toLocaleTimeString('en-US', { hour12: false }).substr(0, 5)
    return todays + " " + times
    // return date.toISOString().substr(0, 10) + " " + date.toLocaleTimeString('en-US', options).substr(0, 5)
}

export default getNowDateTime;