const protectAPI = (handler) => {
    return async (req, res) => {
        console.log("api");
        if (new URL(req.headers.referer).origin !== 'http://yourdomain.com') {
            return res.status(403).json({ success: false, message: `Forbidden` })
        }
        return handler(req, res)
    }
}

export default protectAPI;