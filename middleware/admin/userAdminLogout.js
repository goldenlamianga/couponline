const userAdminLogout = async () => {
    try {
        var userData = localStorage.removeItem("userAdminDataCollection")
        return true
    } catch (error) {
        console.log(error);
        return null
    }

}

export default userAdminLogout;