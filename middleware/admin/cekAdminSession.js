import { hash, compare } from 'bcryptjs'
import Router from "next/router"
import resetAdminSession from './resetAdminSession';

const SESSION_TIME = 1 * 60 * 1000;
const cekAdminSession = async () => {
    const date = new Date();
    date.setDate(date.getDate());
    const dtNow = date.toLocaleDateString()
    // console.log(dtNow)
    const sessionTime = await localStorage.getItem("sessionTimeAdmin")
    console.log("==========cekAdminSession===========>" + sessionTime)
    //Jika sessionTime nya tidak ada maka create baru
    if (!sessionTime) {
        resetAdminSession().then(() => {
            hash(dtNow.toString(), 0).then((rst) => {
                localStorage.setItem("sessionTimeAdmin", rst)
            })
            console.log("Create New sessionTimeAdmin")
        })
    } else {
        //Jika sessionTime nya ada compare date nya, jika date beda hari maka reset data, dan direct to login
        console.log("===========Compare===========>")
        // console.log(dtNow.toString() + " <=> " + localStorage.getItem("sessionTimeAdmin"))
        const match = await compare(dtNow.toString(), localStorage.getItem("sessionTimeAdmin"));
        console.log("=========cekSession==========>" + match)
        if (!match) {
            console.log("Reset All Session")
            resetAdminSession().then(() => {
                hash(dtNow.toString(), 0).then((rst) => {
                    localStorage.setItem("sessionTimeAdmin", rst)
                })
                console.log("Start New sessionTimeAdmin")
                console.log("Direct to onboard")
                return Router.push('/admin/login')
            })
        }
    }
}

export default cekAdminSession;