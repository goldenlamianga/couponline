import encryptObj from "../encdecrypt/encryptObj"

const userAdminLoginStore = async (handler) => {
    try {
        console.log("userAdminLoginStore : ")
        console.log('handler : ')
        console.log(handler)
        var rstCrypt = await encryptObj(handler)
        localStorage.setItem('userAdminDataCollection', rstCrypt);
        return true
    } catch (error) {
        console.log(error);
        return null;
    }
}

export default userAdminLoginStore;