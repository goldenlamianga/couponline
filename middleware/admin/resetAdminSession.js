const resetAdminSession = async (handler) => {
    try {
        localStorage.removeItem('userAdminDataCollection')
        return true
    } catch (error) {
        console.log(error)
        return null;
    }
}

export default resetAdminSession;