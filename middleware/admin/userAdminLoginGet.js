import decryptObj from "../encdecrypt/decryptObj"

const userAdminLoginGet = async () => {
    try {
        var userData = localStorage.getItem("userAdminDataCollection")
        console.log('userAdminLoginGet : ' + localStorage.getItem("userAdminDataCollection"))
        console.log("Cek UserData : " + userData)
        console.log()
        return decryptObj(userData)
    } catch (error) {
        console.log(error);
        return null
    }

}

export default userAdminLoginGet;