import { React, useState, useEffect } from 'react'
import { app, database } from '../firebaseConfig';
import { collection, query, where, addDoc, getDocs } from 'firebase/firestore'
const dbInstance = collection(database, 'state_provinsi');

async function getProject() {
    // project: async () => {
    const provinsi = [];
    try {
        const q = query(dbInstance);
        const querySnapshot = await getDocs(q)
            .then((data) => {
                provinsi.push(data.docs.map((item) => {
                    return { ...item.data(), id: item.id }
                }));
                // console.log("provinsi : ");
                // console.log(provinsi);
                // console.log(data.docs[0].data().nama);
                // const data = [
                //     {
                //         "nama": data.docs[0].data().nama,
                //     }
                // ];
                return provinsi;
            });
        return querySnapshot;
    } catch (error) {
        console.log('GetProject Error : ' + error);
        return null
    }
    // }
}

export default getProject;