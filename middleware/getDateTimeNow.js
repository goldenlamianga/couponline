var date = new Date()
var options = { hour12: false };
const getDateTimeNow = () => {
    return date.toISOString().substr(0, 10) + " " + date.toLocaleTimeString('en-US', options).substr(0, 8)
}

export default getDateTimeNow;