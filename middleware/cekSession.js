import userLocalRemove from "./user/userLocalRemove";
import { hash, compare } from 'bcryptjs'
import Router from "next/router";
import resetDataCollection from "./resetDataCollection";

const SESSION_TIME = 1 * 60 * 1000;
const cekSession = async () => {
    const date = new Date();
    // date.setDate(date.getDate() - 1)
    date.setDate(date.getDate());
    const dtNow = date.toLocaleDateString()
    console.log(dtNow)
    const sessionTime = await localStorage.getItem("sessionTime")
    console.log("==========cekSession===========>" + sessionTime)
    if (!sessionTime) {
        resetDataCollection().then(() => {
            hash(dtNow.toString(), 0).then((rst) => {
                localStorage.setItem("sessionTime", rst)
            })
            console.log("Create New SessionTime")
        })
    } else {
        console.log("===========Compare===========>")
        // console.log(dtNow.toString() + " <=> " + localStorage.getItem("sessionTime"))
        const match = await compare(dtNow.toString(), localStorage.getItem("sessionTime"));
        console.log("=========cekSession==========>" + match)
        if (!match) {
            console.log("Reset All Session")
            resetDataCollection().then(() => {
                hash(dtNow.toString(), 0).then((rst) => {
                    localStorage.setItem("sessionTime", rst)
                })
                console.log("Start New SessionTime")
                console.log("Direct to onboard")
                return Router.push('/onboard')
            })
        }
    }

    // console.log("user_session_times : " + localStorage.getItem("user_session_times"));
    // console.log("hasCart : " + localStorage.getItem("hasCart"));
    // if (localStorage.getItem("isLoggedIn") == "true") {
    // return console.log(localStorage.getItem("sessionTime"))
    // return console.log("==>cekSession - jika ada localstorage paramTrans yang lewat 1hari dari tanggal dia akses maka clear / reset all session localstorage")
    // }
    // try {
    //     var rst = false;
    //     const now = Date.parse(new Date());
    //     console.log('==> cekSession : ' + new Date() + ' : ' + localStorage.getItem("user_session_times"));
    //     if (localStorage.getItem("user_session_times") != null) {
    //         const store = Date.parse(localStorage.getItem("user_session_times"));
    //         const cek = now - store;
    //         console.log("==> " + now + ' - ' + store + ' : ' + cek + ' > ' + SESSION_TIME);
    //         //cek jika time saat ini sudah lebih dari 60 menit dari user login maka logout user account
    //         if (cek > SESSION_TIME) {
    //             console.log('==> get cekSession : ' + localStorage.getItem("user_session_times"));
    //             console.log('==> Lewat 60 Menit Remove session');
    //             userLocalRemove();
    //             rst = false;
    //         } else {
    //             rst = true;
    //         }
    //     } else {
    //         userLocalRemove();
    //         rst = false;
    //     }
    //     return rst;
    // } catch (error) {
    //     console.log('error cek session : ' + error);
    //     return rst = false;
    // }

}

export default cekSession;