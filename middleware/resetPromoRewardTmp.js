const resetPromoRewardTmp = async (handler) => {
    try {
        // localStorage.setItem('tmpReward', null)
        localStorage.setItem('tmpPromo', null)
        return true
    } catch (error) {
        console.log(error)
        return null;
    }

}

export default resetPromoRewardTmp;