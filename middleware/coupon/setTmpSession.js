const setTmpSession = async (promo) => {
    try {
        localStorage.setItem("tmpPromo", JSON.stringify(promo))
    } catch (error) {
        localStorage.setItem("tmpPromo", null)
    }
}

export default setTmpSession;