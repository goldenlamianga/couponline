const getTmpSession = async () => {
    try {
        return JSON.parse(localStorage.getItem("tmpPromo"))
    } catch (error) {
        return null
    }
}

export default getTmpSession