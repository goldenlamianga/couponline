import CryptoJS from 'crypto-js';

const decryptText = async (data) => {
    try {
        var bytes = CryptoJS.AES.decrypt(data, 'secret-key-777');
        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        console.log("decryptedDataText : -> ");
        // console.log(originalText);
        return originalText
    } catch (error) {
        console.log("Error Decrypt Text : ")
        console.log(error)
        return null
    }
}

export default decryptText;