import CryptoJS from 'crypto-js';

const encryptObj = async (data) => {
    try {
        var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), 'secret-key-777').toString();
        console.log("encryptedObj : " + ciphertext)
        return ciphertext
    } catch (error) {
        console.log("Error Encrypt Object : ")
        console.log(error)
        return null
    }
}

export default encryptObj;