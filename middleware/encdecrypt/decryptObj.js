import CryptoJS from 'crypto-js';

const decryptObj = async (data) => {
    try {
        var bytes = CryptoJS.AES.decrypt(data, 'secret-key-777');
        var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        console.log("decryptedDataObj : -> ");
        // console.log(decryptedData);
        return decryptedData
    } catch (error) {
        console.log("Error Decrypt Object : ")
        console.log(error)
        return null
    }
}

export default decryptObj;