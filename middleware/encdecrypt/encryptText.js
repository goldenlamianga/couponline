import CryptoJS from 'crypto-js';

const encryptText = async (data) => {
    try {
        var ciphertext = CryptoJS.AES.encrypt(data, 'secret-key-777').toString();
        console.log("encryptedText : " + ciphertext)
        return ciphertext
    } catch (error) {
        console.log("Error Encrypt Text : ")
        console.log(error)
        return null
    }
}

export default encryptText;