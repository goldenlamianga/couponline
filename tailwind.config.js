const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  mode: 'jit',
  theme: {
    fontFamily: {
      'sans': ['Vazirmatn', ...defaultTheme.fontFamily.sans],
    },
    fontSize: {
      'lxs': '.65rem',
      'xs': '.75rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    backgroundImage: {
      'car-road': "url('https://www.avindatahistory.com/assets/images/vehicle-history-background.jpg')"
    },
    extend: {
      colors: {
        'seven-blue': '#2f52ff',
        'seven-blue-light': '#008cff',
        'seven-red': '#9c1e21',
        'seven-red2': '#ff0000',
        'seven-gray1': '#dbdee2',
        'seven-gray2': '#C1C1C1',
        'seven-gold1': '#7C7C48',
        'seven-gold2': '#7C7C1B',
        'seven-blue-default': '#3e98c7',
        'seven-green-btn_float': '#dc2929',
        'seven-red-btn_float': '#dc2929',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('tailwind-scrollbar-hide')
  ]
}

// seven-blue-light - rgba(0, 140, 255