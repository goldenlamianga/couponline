-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Setting" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "emailNotifikasi" TEXT,
    "email" TEXT,
    "realReport" TEXT NOT NULL DEFAULT '0',
    "name" TEXT NOT NULL DEFAULT 'setting',
    "password" TEXT
);
INSERT INTO "new_Setting" ("createdAt", "email", "emailNotifikasi", "id", "name", "password") SELECT "createdAt", "email", "emailNotifikasi", "id", "name", "password" FROM "Setting";
DROP TABLE "Setting";
ALTER TABLE "new_Setting" RENAME TO "Setting";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
