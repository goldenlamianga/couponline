-- CreateTable
CREATE TABLE "Order" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userEmail" TEXT NOT NULL,
    "userPhone" TEXT NOT NULL,
    "vin" TEXT NOT NULL,
    "userFirstName" TEXT NOT NULL,
    "userLastName" TEXT NOT NULL,
    "created" TEXT NOT NULL,
    "status" TEXT NOT NULL,
    "paypalId" TEXT NOT NULL,
    "payerCountryCode" TEXT NOT NULL,
    "payerEmail" TEXT NOT NULL,
    "payerFirstName" TEXT NOT NULL,
    "payerLastName" TEXT NOT NULL,
    "vinStatus" TEXT NOT NULL DEFAULT 'pending'
);

-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT,
    "password" TEXT,
    "phone" TEXT NOT NULL,
    "role" INTEGER NOT NULL DEFAULT 1
);

-- CreateTable
CREATE TABLE "Customer" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "firstName" TEXT,
    "lastName" TEXT,
    "phone" TEXT NOT NULL,
    "email" TEXT NOT NULL
);
