-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Order" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userEmail" TEXT NOT NULL,
    "userPhone" TEXT NOT NULL,
    "vin" TEXT NOT NULL,
    "userFirstName" TEXT NOT NULL,
    "userLastName" TEXT NOT NULL,
    "created" TEXT NOT NULL,
    "status" TEXT NOT NULL,
    "paypalId" TEXT NOT NULL,
    "payerCountryCode" TEXT NOT NULL,
    "payerEmail" TEXT NOT NULL,
    "payerFirstName" TEXT NOT NULL,
    "payerLastName" TEXT NOT NULL,
    "report" TEXT NOT NULL DEFAULT '',
    "reportId" TEXT NOT NULL DEFAULT '',
    "vinStatus" TEXT NOT NULL DEFAULT 'pending'
);
INSERT INTO "new_Order" ("created", "createdAt", "id", "payerCountryCode", "payerEmail", "payerFirstName", "payerLastName", "paypalId", "status", "userEmail", "userFirstName", "userLastName", "userPhone", "vin", "vinStatus") SELECT "created", "createdAt", "id", "payerCountryCode", "payerEmail", "payerFirstName", "payerLastName", "paypalId", "status", "userEmail", "userFirstName", "userLastName", "userPhone", "vin", "vinStatus" FROM "Order";
DROP TABLE "Order";
ALTER TABLE "new_Order" RENAME TO "Order";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
