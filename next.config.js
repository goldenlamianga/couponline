// module.exports = {
//   reactStrictMode: false,
//   images: {
//     loader: 'akamai',
//     path: '',
//   },
//   env: {
//     ADMIN_URL: process.env.ADMIN_URL,
//     AUTOVIN_TOKEN: process.env.AUTOVIN_TOKEN,
//     PAYPAL_CLIENT: process.env.PAYPAL_CLIENT,
//     ADS_TIME: process.env.ADS_TIME,
//     MID_SERVER_KEY: process.env.MID_SERVER_KEY,
//     MID_CLIENT_KEY: process.env.MID_CLIENT_KEY,
//     MID_URL_CEK_STATUS: process.env.MID_URL_CEK_STATUS,
//   }
// }
module.exports = {
  env: {
    node: true,
    browser: true,
    es2021: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@next/next/recommended",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    "react/no-unknown-property": ["error", { ignore: ["jsx"] }],
  },
  globals: {
    React: "writable",
  },
  settings: {
    react: {
      version: "detect",
    },
  },
    reactStrictMode: false,
  images: {
    loader: 'akamai',
    path: '',
  },
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      "/": { page: "/" },
      "/admin": { page: "/admin" },
    }
  },
  trailingSlash: true,
}