import { React, useState } from 'react'
import axios from 'axios'
import { FaArrowRight } from "react-icons/fa"
import Router from 'next/router'
import { motion } from "framer-motion"

function SubFooter() {
    const [vin, setVin] = useState("")
    const [data, setData] = useState("")
    const [car, setCar] = useState({})
    const [loading, setLoading] = useState(false)
    const [isWrong, setIsWrong] = useState(false)
    const [reportShown, setReportShown] = useState(false)
    const [reportBlank, setReportBlank] = useState(false)

    const variants = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };

    function handleChange(e) {
        setIsWrong(false);
        setVin(e.target.value);
    }


    async function checkVin() {
        setCar({});
        setIsWrong(false);
        setReportBlank(false);
        if (vin.length != 17 || vin.toUpperCase().includes("O") || vin.toUpperCase().includes("I") || vin.toUpperCase().includes("Q")) {
            setIsWrong(true);
            return;
        }
        try {
            setLoading(true);
            var res = await axios.post(`/api/vin-decode`, {
                vin: vin
            });
            setCar(res.data)
            if (res.data.Available) {
                setReportShown(true);
            } else {
                setReportBlank(true);
            }
            setLoading(false);

        } catch (error) {
            console.log(error);
            setLoading(false);
            setReportShown(false);
            setReportBlank(true);
        }
    }
    return (
        <div className="w-screen bg-[url('https://images.unsplash.com/photo-1504887764023-6f27056d186c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=922&q=80')] bg-center bg-cover overflow-hidden bg-fixed">
            <div className="w-full bg-black bg-opacity-50 py-16 lg:px-10 px-3">
                <div className='text-2xl text-white font-bold text-center'>Vehicle Research Made Easy</div>
                <div className='text-white text-center text-sm'>ENTER A VIN BELOW TO GET STARTED</div>
                <motion.div transition={{ delay: 0.1 }} initial={{ opacity: 0 }} animate={{ opacity: 1 }} className='flex flex-col justify-between w-full sm:w-full md:w-full lg:w-1/2 mt-5 text-center shadow-lg rounded  mx-auto'>


                    <motion.div variants={variants} transition={{ delay: 0.1 }} animate={!reportShown ? "visible" : "hidden"}>
                        <input maxLength="17" value={vin} onChange={(e) => handleChange(e)} className="rounded-l-lg p-4 w-7/12 border-t mr-0 border-b border-l text-gray-800 border-white bg-gray-100 focus:outline-none focus:border-sky-500" placeholder="Enter your 17 character VIN" />
                        <button onClick={() => checkVin()} className="px-0 sm:px-6 w-3/12 rounded-r-lg bg-sky-400  text-white font-bold p-4 uppercase border-blue-400 border-t border-b border-r">{loading ? "CHECKING..." : "CHECK"}</button>
                    </motion.div>
                    {isWrong && <motion.div transition={{ delay: 0.1 }} animate={{ opacity: [0, 1, 0, 1] }} className='text-sm font-semibold text-red-500 mt-0 w-10/12 mx-auto text-center'>
                        Valid vehicle identification number have exactly 17 characters
                        Came from a vehicle manufactured since 1981
                        Do not contain letters I, O, or Q.
                    </motion.div>}
                    {reportBlank && <motion.div transition={{ delay: 0.1 }} animate={{ opacity: [0, 1, 0, 1] }} className='bg-orange-50 p-3 rounded-md mt-2 mb-2 text-xs font-semibold text-orange-500 w-10/12 mx-auto text-left'>
                        <span className='font-bold'>We&apos;re sorry: We can&apos;t find that car</span><br />
                        Was the car manufactured before 1981? Then it doesn&apos;t have the VIN we need to find the answers you&apos;re looking for.
                        Is the car brand new? It may be so new that there are no records currently on file. Check back soon, once it&apos;s had some time on the road.
                    </motion.div>}
                    <motion.div variants={variants} transition={{ delay: 0.1 }} animate={reportShown ? "visible" : "hidden"} className='rounded-lg border-2 bg-sky-100 p-3 w-10/12 mx-auto shadow-md my-3 border-sky-400'>
                        <div className='text-sm'>
                            Get the full report to learn more:
                        </div>
                        <div className='font-bold text-xl'>
                            {car?.Vehicle?.split("/")[0]}
                        </div>
                        <div className='text-xs'>
                            VIN: <span className='font-bold'>{vin?.toUpperCase()}</span>
                        </div>
                        <div className='w-10/12 mx-auto'>
                            <button onClick={() => Router.push({ pathname: '/vehicle-report', query: { vin: vin } })} className="flex items-center justify-center w-full rounded-lg bg-sky-500  text-white font-bold p-3 uppercase border-cyan-500 border-t border-b border-r mt-4"><span className='mr-2'>get full report</span> <FaArrowRight /></button>
                        </div>
                    </motion.div>
                    <motion.button variants={variants} transition={{ delay: 0.1 }} animate={reportShown ? "visible" : "hidden"} onClick={() => setReportShown(false)} className="flex items-center justify-center w-10/12 mx-auto rounded-lg bg-yellow-500  text-white font-bold p-3 uppercase border-yellow-500 border-t border-b border-r mt-0 mb-3"><span className=''>Search Again</span></motion.button>

                </motion.div>
            </div>
        </div>
    )
}

export default SubFooter
