import { useEffect, useState, React } from 'react'
import { useSession, signIn, signOut } from "next-auth/react"
import Router from 'next/router'
import axios from 'axios'
import { motion } from 'framer-motion';

function AdminSidebar(props) {
    const [balance, setBalance] = useState("");
    const { data: session } = useSession()

    useEffect(() => {
        getBalance();
    }, [])

    async function getBalance() {
        try {
            var result = await axios.get(`https://cgi.autovin.de/client/checkbalance?authtoken=${process.env.AUTOVIN_TOKEN}`);
            setBalance(result.data);
        } catch (error) {
            console.log(error);
        }
    }

    const { selected } = props;
    return (
        <div className='border rounded-md bg-white mr-3 w-2/12 p-3'>

            {session && <div>
                <motion.div animate={{ opacity: [0, 1] }} className='text-xs bg-sky-100 p-1 px-2 rounded-md'>Balance: <span className='font-bold'>{balance}</span></motion.div>
                <div onClick={() => Router.push("/admin-view-order")} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 1 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                </svg>Active</div>
                <div onClick={() => Router.push("/admin-view-order-complete")} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 2 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>Completed</div>
                <div onClick={() => Router.push("/admin-view-customer")} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 3 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M9 6a3 3 0 11-6 0 3 3 0 016 0zM17 6a3 3 0 11-6 0 3 3 0 016 0zM12.93 17c.046-.327.07-.66.07-1a6.97 6.97 0 00-1.5-4.33A5 5 0 0119 16v1h-6.07zM6 11a5 5 0 015 5v1H1v-1a5 5 0 015-5z" />
                </svg>Customers</div>

                <div onClick={() => Router.push("/admin-view-contact")} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 4 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                </svg>Contact Form</div>

                <div onClick={() => Router.push("/admin-edit-profile")} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 5 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                </svg>Settings</div>
                <div onClick={() => signOut({ callbackUrl: `/manageorders` })} className={`py-2 border-b flex items-center text-md cursor-pointer ${selected == 6 ? "font-bold" : "font-normal"}`}><svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                </svg>Logout</div>
            </div>}
        </div>
    )
}

export default AdminSidebar
