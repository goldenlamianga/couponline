import { React, useState, useEffect } from 'react'
import RewardCard from './reward-card'
import { motion } from "framer-motion"
import axios from 'axios'

function RewardContainer() {
    const [shownPage, setShownPage] = useState(false)
    const [loading, setLoading] = useState(false)
    const [reward, setDataReward] = useState([])


    const variants = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };

    async function getReward() {
        console.log("=========GET-REWARD=============>")
        try {
            setLoading(true);
            var res = await axios.get(`/api/reward/get-reward`);
            // console.log(res.data.success)
            // console.log(res.data.data)

            if (res.data.success) {
                setDataReward(res.data.data)
                if (res.data.data.length > 0) setShownPage(true)
            } else {

            }
            setLoading(false);

        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }

    useEffect(() => {
        getReward()
    }, [])

    if (shownPage) {
        return (
            <div className='w-screen overflow-hidden pb-10 shadow-sm'>
                <motion.div transition={{ delay: 0.1 }} initial={{ opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                    <div className='pt-2'>
                        <div className='px-3'>
                            <label className='text-sm font-semibold'>Rewards</label>
                        </div>
                    </div>
                    <div className="w-full">
                        <div className="overflow-x-auto flex h-auto">
                            {reward.map((data, index) => (
                                <RewardCard key={index} data={data} image="https://t3.ftcdn.net/jpg/03/14/04/52/360_F_314045258_CqMzlGsObnRCJd5wmJHVNo4g4F86epeh.jpg" />
                            ))}
                            {/* <RewardCard image="https://t3.ftcdn.net/jpg/03/14/04/52/360_F_314045258_CqMzlGsObnRCJd5wmJHVNo4g4F86epeh.jpg" />
                            <RewardCard image="https://t3.ftcdn.net/jpg/03/14/04/52/360_F_314045258_CqMzlGsObnRCJd5wmJHVNo4g4F86epeh.jpg" />
                            <RewardCard image="https://t3.ftcdn.net/jpg/03/14/04/52/360_F_314045258_CqMzlGsObnRCJd5wmJHVNo4g4F86epeh.jpg" />
                            <RewardCard image="https://t3.ftcdn.net/jpg/03/14/04/52/360_F_314045258_CqMzlGsObnRCJd5wmJHVNo4g4F86epeh.jpg" /> */}
                        </div>
                    </div>
                </motion.div>
            </div >
        )
    }
    return (
        <div className="flex flex-col xl:flex-row justify-center items-center w-full h-32 overflow-hidden cursor-pointer">
        </div>
    )
}

export default RewardContainer
