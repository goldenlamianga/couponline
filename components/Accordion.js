import React, { useState } from "react";
import AccordionItem from "./AccordionItem";

const Accordion = ({ questionsAnswers }) => {
    const [activeIndex, setActiveIndex] = useState(1);

    const renderedQuestionsAnswers = questionsAnswers.map((item, index) => {
        const showDescription = index === activeIndex ? "show-description" : "";
        const fontWeightBold = index === activeIndex ? "font-weight-bold" : "";
        const ariaExpanded = index === activeIndex ? "true" : "false";
        return (
            <AccordionItem
                key={index}
                showDescription={showDescription}
                fontWeightBold={fontWeightBold}
                ariaExpanded={ariaExpanded}
                item={item}
                index={index}
                onClick={() => {
                    setActiveIndex(index);
                }}
            />
        );
    });

    return (
        <div className="w-full grid grid-cols-1 lg:grid-cols-2 py-12 gap-4 lg:px-20 px-3 content-center">
            <div className="hidden lg:block">
                <img className="w-100" src="/19333426.jpg" />
            </div>
            <div className="">
                <h1 className="faq__title text-center lg:text-left">FAQ</h1>
                <dl className="faq__list">{renderedQuestionsAnswers}</dl>
            </div>
        </div>

    );
};

export default Accordion;