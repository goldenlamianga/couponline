import { React, useState, useEffect } from 'react'
import Router from 'next/router'
import setTmpSession from '../middleware/coupon/setTmpSession'
import 'keen-slider/keen-slider.min.css'

function PromoCard(props) {

    let onclickHandler = async (e) => {
        console.log("parse OnClick Dialog ID : ")
        props.onClick('paper')
        return
        // console.log("Hola Click Promo : " + props.data.id)
        // await setTmpSession(props.data)
        // Router.push({ pathname: '/coupon/detail', query: { id: props.data.id } })
    }
    return (
        <div className="flex flex-col xl:flex-row shadow-sm hover:shadow-sm 
                lg:w-full sm:w-full
                lg:h-80 sm:h-80
                w-96
                overflow-hidden
                rounded-lg border-2 border-gray-50 cursor-pointer keen-slider__slide" onClick={() => onclickHandler()}>
            <img className="lg:w-80 sm:w-80 w-full lg:h-full sm:h-full h-full rounded-lg border-2 border-gray-50 " src={props.image} alt=""
            />
        </div>

    )
}

export default PromoCard
