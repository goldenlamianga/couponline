import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

function Footer() {
    return (
        <footer className="text-gray-600 body-font bg-gray-900 footer">
            <div className="container px-5 py-8 mx-auto flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
                <div className="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                    <a className="font-medium flex justify-start">
                        <img src="/logo.png"  alt="Logo" className="h-16" />
                    </a>
                </div>
                <div className="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center">
                    <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                        <h2 className="title-font font-bold text-white tracking-widest text-md mb-3">Company Info</h2>
                        <nav className="list-none mb-10">
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/about-us">About AUTOVINNER Products</Link>
                            </li>
                        </nav>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                        <h2 className="title-font font-bold text-white tracking-widest text-md mb-3">Navigation</h2>
                        <nav className="list-none mb-10">
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/vehicle-report">Vehicle History</Link>
                            </li>
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/sample-report">Sample Report</Link>
                            </li>
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/pricing">Pricing</Link>
                            </li>

                        </nav>
                    </div >
                    <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                        <h2 className="title-font font-bold text-white tracking-widest text-md mb-3">Need Help?</h2>
                        <nav className="list-none mb-10">
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/faq">FAQs</Link>
                            </li>
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/contact-us">Contact Us</Link>
                            </li>

                        </nav >
                    </div >
                    <div className="lg:w-1/4 md:w-1/2 w-full px-4">
                        <h2 className="title-font font-bold text-white tracking-widest text-md mb-3">Other</h2>
                        <nav className="list-none mb-10">
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/term-and-condition">Term & Condition</Link>
                            </li>
                            <li className='cursor-pointer text-white hover:text-cyan-500 transition-all'>
                                <Link href="/privacy-policy">Privacy Policy</Link>
                            </li>
                        </nav >
                    </div >
                </div >
            </div >
            <div className='flex lg:justify-end justify-center pr-5 pb-5'><img src='secured.png' className='w-36' /></div>

            <div className="bg-black py-2">
                <p className="text-white text-sm text-center">AUTOVINNER ©2021
                </p>

            </div>
        </footer >
    )
}

export default Footer
