import { React, useState, useEffect, useRef } from 'react'
import PromoCard from './coupon-card'
import { motion } from "framer-motion"
import axios from 'axios'
import Router from 'next/router'
import ReactLoading from "react-loading"
import { RiCloseCircleLine } from 'react-icons/ri';
import { FaCheck } from 'react-icons/fa';

import Typography from '@mui/material/Typography'
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';

import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, orderBy } from 'firebase/firestore'

import 'keen-slider/keen-slider.min.css'
import KeenSlider from 'keen-slider'
import { useKeenSlider } from "keen-slider/react"


const dbInstance = collection(database, '/Brand/golden/direct_coupon')

function CouponContainer() {
    const [shownPage, setShownPage] = useState(true)
    const [loading, setLoading] = useState(false)
    const [coupons, setDataCoupon] = useState([])
    const [couponOthers, setDataCouponOthers] = useState([])

    const [open, setOpen] = useState(false);
    const [scroll, setScroll] = useState('paper');

    const [selectedTitle, setSelectedTitle] = useState("")
    const [selectedDetail, setSelectedDetail] = useState("")
    const [selectedImg, setSelectedImg] = useState("")
    const [selectedData, setSelectedData] = useState("")

    const [shownInputNumber, setShownInputNumber] = useState(false)
    const [no_hp, setNoHp] = useState("")
    const [isError, setIsError] = useState(false)
    const [error, setError] = useState("")
    const [msgError, setMsgError] = useState("Masukan no handphone untuk dapatkan coupon")
    const [loadingClaim, setLoadingClaim] = useState(false)

    const [showBtnRedeem, setShowBtnRedeem] = useState(true)

    const [shownMsgStatus, setShownMsgStatus] = useState(false)
    const [msgStatus, setMsgStatus] = useState("")
    const [timoutSuccess, setTimoutSuccess] = useState(null)

    const variants = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };

    const [sliderRef] = useKeenSlider({
        mode: "free-snap",
        breakpoints: {
            "(min-width: 400px)": {
                slides: { perView: 2, spacing: 5 },
            },
            "(min-width: 700px)": {
                slides: { perView: 2.5, spacing: 5 },
            },
            "(min-width: 1000px)": {
                slides: { perView: 3.5, spacing: 10 },
            },
        },
        slides: {
            perView: 2,
            spacing: 10,
        },
    });

    const handleClickOpen = (data) => () => {
        setOpen(true);
        setShowBtnRedeem(true)
        setMsgStatus("")
        setSelectedData(data);
        setSelectedImg(data.img);
        setSelectedTitle(data.title);
        var textViewtext = data.detail.replace(/_b/g, "\n")
        setSelectedDetail(textViewtext);
        setShownMsgStatus(false)
    };

    const handleClose = () => {
        setOpen(false);
        setShowBtnRedeem(true)
        setMsgStatus("")
        setShownMsgStatus(false)
    };
    const handleRedem = async () => {
        setError("show")
        setIsError(false)
        setMsgError("")

        setMsgStatus("")
        setShownMsgStatus(false)
        // try {
        //     clearTimeout(timoutSuccess);
        // } catch (error) { }

        // if (no_hp == "") {
        //     setIsError(true)
        //     setError("")
        //     setMsgError("Masukan no handphone untuk dapatkan coupon")
        // } else {
        //     try {
        //         var rst = no_hp.match(/\d/g).length >= 10;
        //         if (!rst) {
        //             setError("")
        //             setIsError(true)
        //             setMsgError("Format nomor handphone kurang")
        //         } else {
        //             selectedData.no_hp = no_hp
        //             setLoadingClaim(true);
        //             var cek = await cekClaimCoupon(selectedData)
        //             if (cek.success) {
        //                 var cek = await prosesClaim(selectedData)
        //                 if (true) {
        setMsgStatus("Redeem berhasil terima kasih, tolong tunjukan ini ke kasir.")
        setShownMsgStatus(true)
        setShowBtnRedeem(false)
        //             }

        //         } else {
        //             setError("")
        //             setIsError(true)
        //             setMsgError(cek.message)
        //         }
        //         setLoadingClaim(false);
        //     }
        // } catch (error) {
        //     setError("")
        //     setIsError(true)
        //     setMsgError("Format nomor handphone salah")
        //     setLoadingClaim(false);
        // }
        // }
    };

    function doneRedeem() {
        resetData();
        setOpen(false);
    }

    const resetData = () => {
        setNoHp("")

        setError("show")
        setIsError(false)
        setMsgError("")

        setShownMsgStatus(false)
        setMsgStatus("")

        setLoadingClaim(false);
    }

    const descriptionElementRef = useRef(null);
    useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);


    async function getCoupon() {
        try {
            setLoading(true);
            var itemColl = []
            const q = query(dbInstance, 
                where('status', '==', 0), 
                where('brand', '==', 'gl'), orderBy("id"));
            const querySnapshot = await getDocs(q).then((data) => {
                if (!data.empty) {
                    data.docs.map((item) => {
                        itemColl.push({ ...item.data(), coupon_id: item.id })
                    });
                }
            });
            setLoading(false);
            setDataCoupon(itemColl)
        } catch (error) {
            setLoading(false);
            console.log(error);
        }
    }

    async function getCouponOthers() {
        try {
            setLoading(true);
            var itemColl = []
            const q = query(dbInstance, 
                where('status', '==', 0), 
                where('brand', "in", ['tea', 'ghg', 'sozo']),
                orderBy("id"));
            const querySnapshot = await getDocs(q).then((data) => {
                if (!data.empty) {
                    data.docs.map((item) => {
                        itemColl.push({ ...item.data(), coupon_id: item.id })
                    });
                }
            });
            setLoading(false);
            setDataCouponOthers(itemColl)
        } catch (error) {
            setLoading(false);
            console.log(error);
        }
    }
    

    async function cekClaimCoupon(data) {
        try {
            var itemColl = []
            var coupon_id = data.coupon_id
            var no_hp = data.no_hp
            var max_redem = data.max_redem
            var periode_start = data.periode_start
            var periode_end = data.periode_end
            var data_size = 0
            const dbInstanceClaim = collection(database, '/Brand/golden/direct_coupon_claim')
            const q = query(dbInstanceClaim,
                where('phone_number', '==', no_hp),
                where('id_coupon', '==', coupon_id)
            );
            const querySnapshot = await getDocs(q).then((dataRst) => {
                if (!dataRst.empty) {
                    dataRst.docs.map((item) => {
                        itemColl.push({ ...item.data(), data: dataRst.size })
                    });
                    data_size = dataRst.size
                }
            });
            if (data_size > 0) {
                return { success: false, message: "Maaf, Sudah pernah redeem", data: "", error: null }
            } else {
                return { success: true, message: "", data: data_size, error: null }
            }

        } catch (error) {
            console.log(error);
            return { success: false, message: "Gagal, coba kembali lagi.", data: "", error: error }
        }
    }

    async function prosesClaim(data) {
        var coupon_id = data.coupon_id
        var no_hp = data.no_hp
        var salesArea = data.SalesArea

        const modelCoupon = {
            "id_coupon": coupon_id,
            "SalesArea": salesArea,
            "phone_number": no_hp,
            "status": 0,
            "createdAt": new Date(),
            "createdBy": new Date(),
            "updatedAt": new Date(),
            "updatedBy": new Date()
        }
        try {
            const dbInstanceClaim = collection(database, '/Brand/golden/direct_coupon_claim')
            const q = query(dbInstanceClaim);
            const sendData = await addDoc(dbInstanceClaim, modelCoupon).then((data) => {
                if (!data.empty) {
                    return { success: true, message: data.id, error: null }
                } else {
                    return { success: false, message: data, error: null }
                }
            });
        } catch (error) {
            console.log(error);
            return { success: false, message: "something went wrong", error: error }
        }
    }

    useEffect(() => {
        getCoupon()
        getCouponOthers()
    }, [])

    useEffect(() => {
    }, [error, isError, no_hp, loading, timoutSuccess])

    if (shownPage) {
        return (
            <div className="w-full flex justify-center items-center">
                <Dialog
                    open={open}
                    onClose={handleClose}
                    id="no-scrollbar"
                    PaperProps={{
                        style: {
                            backgroundColor: 'transparent',
                            boxShadow: 'none',
                        },
                    }}
                >
                    <div className="bg-transparent w-full text-black flex justify-end">
                        <button onClick={handleClose}>
                            <RiCloseCircleLine size={30} className='text-white' />
                        </button>
                    </div>
                    {/* <DialogTitle id="scroll-dialog-title" ></DialogTitle> */}
                    <DialogContent dividers={scroll === 'paper'} className='bg-white rounded-t-md'>
                        <div className='flex justify-center bg-white'>
                            <img
                                className="object-cover mb-3 rounded-md 
                                        lg:w-80 sm:w-80 
                                        w-full
                                        lg:h-full sm:h-full
                                        h-full "
                                src={(selectedImg != "") ? selectedImg : "/assets/logo-sq.jpg"}
                                alt=""
                            />
                        </div>
                        <div className='font-semibold text-lg'>
                            {selectedTitle}
                        </div>
                        <Typography
                            variant="body1"
                            style={{ whiteSpace: 'pre-line' }}
                        >
                            {selectedDetail}
                        </Typography>

                    </DialogContent>
                    <DialogActions className='bg-white rounded-b-md px-2'>
                        <div className='w-full bg-red flex flex-col justify-center font-bold'>
                            <div className='w-full lg:px-16 md:px-16 px-1 p-2 '>
                                <motion.div variants={variants} transition={{ delay: 0.1 }} initial={{ opacity: 0 }} animate={shownMsgStatus ? "visible" : "hidden"}>
                                    <div className="w-full px-2 py-3 mb-2 bg-green-100 rounded-md text-center flex flex-row">
                                        <FaCheck size={20} className='text-green-600 mx-1' />
                                        {msgStatus}
                                    </div>
                                </motion.div>
                            </div>
                            {/* <div className="w-full px-2 pb-2 bg-gray-100 rounded-md">
                                <TextField
                                    margin="dense"
                                    id="name"
                                    label="No. Handphone"
                                    type="text"
                                    fullWidth
                                    variant="standard"
                                    onChange={event => setNoHp(event.target.value)}
                                    error={error === ""}
                                    helperText={msgError}
                                    inputProps={{ maxLength: 12 }}
                                />
                            </div> */}
                            <div className='w-full'>
                                <motion.div variants={variants} transition={{ delay: 0.1 }} initial={{ opacity: 0 }} animate={showBtnRedeem ? "visible" : "hidden"}>
                                    <button className="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded tracking-wider w-full" onClick={handleRedem}>
                                        {loadingClaim ? "Loading..." : "REDEEM"}
                                    </button>
                                </motion.div>
                            </div>
                        </div>
                    </DialogActions>
                </Dialog>

                <div className="bg-white w-full h-screen relative z-0">
                    <img src="/assets/pattern.png" className="object-cover w-full h-full opacity-50" />

                    <motion.div transition={{ delay: 0.1 }} initial={{ opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                        <div className="absolute inset-0 flex flex-col justify-start items-center z-10 ">
                            <div className='px-3 pb-1 text-center'>
                                <img src="/assets/coupon-label.png" className="object-cover scale-75" />
                            </div>
                            <div className="w-full">
                                {(coupons.length > 0) ?
                                    <><div ref={sliderRef} className="keen-slider pb-3">
                                        {coupons.map((data, index) => (
                                            <PromoCard key={index} data={data} image={(data.img != "") ? data.img : "/assets/logo-sq.jpg"} onClick={handleClickOpen(data)} />
                                        ))}
                                    </div></>

                                    :
                                    <div id="no-scrollbar" className="w-full pt-10 text-center">
                                        Belum ada data.
                                    </div>
                                }
                                 {(couponOthers.length > 0) ?
                                    <><div ref={sliderRef} className="keen-slider pb-3">
                                        {couponOthers.map((data, index) => (
                                            <PromoCard key={index} data={data} image={(data.img != "") ? data.img : "/assets/logo-sq.jpg"} onClick={handleClickOpen(data)} />
                                        ))}
                                    </div></>

                                    :
                                    <div id="no-scrollbar" className="w-full pt-10 text-center">
                                        Belum ada data.
                                    </div>
                                }
                            </div>

                        </div>


                    </motion.div>
                </div >
            </div >
        )
    }
    return (
        // <div className='flex justify-center items-center h-screen'>
        <div className="flex flex-col xl:flex-row justify-center items-center w-full h-32 overflow-hidden cursor-pointer">
            <ReactLoading type="bubbles" color="#4287f5" />
        </div>
        // </div>
    )
}

export default CouponContainer
