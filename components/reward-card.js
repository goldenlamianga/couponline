import { React, useState, useEffect } from 'react'
import Router from 'next/router'
import setRewardTmpSession from '../middleware/reward/setRewardTmpSession'


function RewardCard(props) {
    // console.log("==========REWARD-CARD=======>" + props.data.id)
    // console.log(props.data.id)
    // console.log(props)
    let onclickHandler = (e) => {
        console.log("Hola Click Reward : " + props.data.id)
        setRewardTmpSession(props.data)
        Router.push({ pathname: '/reward', query: { id: props.data.id } })
    }
    return (
        <div className="flex-none pb-6 px-2 first:pl-3 last:pr-6">
            <button onClick={() => onclickHandler()} >
                <div className="flex flex-col xl:flex-row shadow-sm hover:shadow-sm w-full h-32 rounded-lg border-2 border-gray-50 overflow-hidden cursor-pointer">
                    <img
                        className="object-cover h-32 w-36"
                        src={props.data.img}
                        alt="reward-card"
                    />
                </div>
            </button>
        </div >

    )
}

export default RewardCard
