import { React, useEffect, useState, Fragment } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import Button from '@mui/material/Button';


function DialogCustom(props) {
    const [data, setData] = useState(null);
    const [idx, setIdx] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [title, setTitle] = useState("");
    const [messageBody, setMessageBody] = useState("");


    const handleClose = () => {
        setIsOpen(false);
        props.setIsOpen(false)
    };

    const handleOke = () => {
        console.log(data)
        console.log(idx)
        props.setIsOke(true)
        props.setIsOpen(false)
    };

    useEffect(() => {
        if (props.data) {
            setData(props.data);
            setIdx(props.idx);
            setIsOpen(props.isOpen);
            setTitle(props.title);
            setMessageBody(props.messageBody);
        }
    }, [])

    useEffect(() => {
        // props.onSelected({ ...data, index: props.idx });
        // props.setIsOpen(isOpen);
        console.log("Dialog isOpen : " + isOpen)
    }, [data, idx, isOpen, title, messageBody])


    return (
        <Dialog
            open={props.isOpen ?? false}
            // open={isOpen}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {title + "(" + idx + ")"}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {messageBody}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleOke} autoFocus>
                    Ok
                </Button>
            </DialogActions>
        </Dialog>
    )
}
export default DialogCustom
