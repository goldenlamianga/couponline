import { useEffect, useState } from 'react';
import userAdminLoginGet from '../../middleware/admin/userAdminLoginGet';

function Header() {
    const [dataUserLogin, setDataUserLogin] = useState([])

    const getDataUserAdminLogin = async () => {
        var data = await userAdminLoginGet()
        // console.log("Data Header User Admin Login : ", data)
        if (data) {
            setDataUserLogin(data)
        }
    }

    useEffect(() => {
        getDataUserAdminLogin()
    }, []);

    return (
        <div className='bg-slate-100 grow h-14 flex justify-between items-center text-slate-800 border-b px-10 space-x-2 text-sm no-print'>
            <div className='flex justify-start space-x-2 items-center font-semibold pl-3'>
                {/* <a href='#'>Menu 1</a> */}
                {/* <a href='#'>Menu 2</a> */}
                {/* GOLDEN LAMIAN KOTA KASABLANKA */}
                {/* {"OutletID : " + (dataUserLogin.SalesAreaID ?? " - ")} */}
            </div>
            <div className='flex justify-end space-x-2 items-center'>
                <img className='w-7 rounded-full' src='https://www.w3schools.com/howto/img_avatar2.png' />
                <div>{dataUserLogin.nama ?? " - "}</div>
            </div>
        </div>
    )
}

export default Header