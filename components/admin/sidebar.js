import { motion } from "framer-motion";
import Router from 'next/router';
import { useEffect, useState } from 'react';
import { IconContext } from 'react-icons';
import { FcMenu } from 'react-icons/fc';
import { RiCoupon3Line } from 'react-icons/ri';
import { IoMdArrowDropdown, IoMdArrowDropleft } from 'react-icons/io';
import userAdminLogout from '../../middleware/admin/userAdminLogout';

function Sidebar(props) {
    const [active, setActive] = useState(null);
    const [isShowMenu, setShowMenu] = useState(true);

    const variants = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, x: 0, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };

    const variantsSubMenu = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, x: 10, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };

    const [subMenuContentMaster, setSubMenuContentMaster] = useState(false);
    const menuContentMaster = () => {
        setSubMenuContentMaster(!subMenuContentMaster)
    }

    const [subMenuBrand, setSubMenuBrand] = useState(false);
    const menuBrand = () => {
        setSubMenuBrand(!subMenuBrand)
    }

    useEffect(() => {
        setActive(props.active);
        buildMenu(props.active)
    }, []);

    const buildMenu = (menuName) => {
        //add menu collection & selected
        if (menuName == "/outlet") {
            setSubMenuSetting(true)
        } else if (menuName == "/storebanner" || menuName == "/advertisement") {
            setSubMenuContentMaster(true)
        }
        else if (menuName == "/menusequencing" || menuName == "/promotions" || menuName == "/store" || menuName == "/category_menu" || menuName == "/table") {
            setSubMenuBrand(true)
        } else if (menuName == "coupons") {

        }
    }

    const showHideMenu = () => {
        setShowMenu(!isShowMenu)
    }
    const handleClick = (link) => {
        Router.push(link);
    }
    const logout = () => {
        userAdminLogout()
        Router.push('/admin/login')
        return;
    }
    return (
        <div className={`${!isShowMenu && "font-bold w-1"} side-bar min-h-screen antialiased text-gray-800 no-print"`}>
            <div className={`${isShowMenu && "side-bar flex flex-col w-64 bg-slate-100 h-full border-r min-h-screen"} bg-slate-100 no-print`}>
                <div className="flex items-center justify-between w-auto h-14 border-b px-3 ">
                    <motion.div variants={variants} transition={{ delay: 0.1 }} animate={isShowMenu ? "visible" : "hidden"}>
                        <div className='text-sm'><span className='text-indigo-700 font-bold text-lg pr-2 mr-1 border-r'>7N</span> Coupons Dashboard</div>
                    </motion.div>
                    <a onClick={() => showHideMenu()} href="#" className={`${!isShowMenu && "font-bold bg-slate-200"} relative flex flex-row items-center bg-slate focus:outline-none hover:bg-slate-300 rounded-sm text-gray-600 hover:text-gray-800 p-2`}>
                        <IconContext.Provider value={{ color: "blue", className: "text-red-400" }}>
                            <FcMenu size={20} color="red" />
                        </IconContext.Provider>
                    </a>
                </div>
                <motion.div variants={variants} transition={{ delay: 0.05 }} animate={isShowMenu ? "visible" : "hidden"}>
                    <div className="overflow-y-auto overflow-x-hidden flex-grow">
                        <ul className="flex flex-col py-0 space-y-1">
                            {/* <li className="px-5">
                                <div className="flex flex-row items-center h-8">
                                    <div className="text-sm font-light tracking-wide text-gray-500">General</div>
                                </div>
                            </li> */}
                            <li>
                                <a onClick={() => handleClick("/admin")} href="#" className={`${active == "dashboard" && "font-bold border-indigo-500 bg-gray-50"} relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-gray-600 hover:text-gray-800 border-l-4 border-transparent hover:border-indigo-500 pr-6`}>
                                    <span className="inline-flex justify-center items-center ml-4">
                                        <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                                    </span>
                                    <span className="ml-2 text-sm tracking-wide truncate">Dashboard</span>
                                </a>
                            </li>
                            <li>
                                <a onClick={() => handleClick("/admin/coupons")} href="#" className={`${active == "coupons" && "font-bold border-indigo-500 bg-gray-50"} relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-gray-600 hover:text-gray-800 border-l-4 border-transparent hover:border-indigo-500 pr-6`}>
                                    <span className="inline-flex justify-center items-center ml-4">
                                        <IconContext.Provider value={{ color: "blue", className: "text-red-400" }}>
                                            <RiCoupon3Line size={20} color="currentColor" />
                                        </IconContext.Provider>
                                    </span>
                                    <span className="ml-2 text-sm tracking-wide truncate">Coupons</span>
                                </a>
                            </li>
                            <li>
                                <a onClick={() => logout()} href="#" className={`${active == "logout" && "font-bold"} relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-50 text-gray-600 hover:text-gray-800 border-l-4 border-transparent hover:border-indigo-500 pr-6`}>
                                    <span className="inline-flex justify-center items-center ml-4">
                                        <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path></svg>
                                    </span>
                                    <span className="ml-2 text-sm tracking-wide truncate">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </motion.div>
            </div>
        </div>
    )
}

export default Sidebar