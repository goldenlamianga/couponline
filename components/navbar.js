import { React, useState, useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

function Navbar() {
    const [mobileMenu, setMobileMenu] = useState(false);

    function toggleMenu() {
        setMobileMenu(!mobileMenu);
    }

    const router = useRouter()

    const handleScroll = () => {
        if (window.scrollY > 80) {
            document.querySelector(".navbar").className = "navbar scroll shadow";
        } else {
            document.querySelector(".navbar").className = "navbar border-b";
        }
    };

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, [])

    useEffect(() => {
        const handleRouteChange = (url) => {
            setMobileMenu(false);
        }

        router.events.on('routeChangeStart', handleRouteChange)

        // If the component is unmounted, unsubscribe
        // from the event with the `off` method:
        return () => {
            router.events.off('routeChangeStart', handleRouteChange)
        }
    }, [])


    const [inputText, setInputText] = useState("");
    let inputHandler = (e) => {
        //convert input text to lower case
        var lowerCase = e.target.value.toLowerCase();
        setInputText(lowerCase);
    };

    return (
        <nav className="navbar bg-white z-50 border-b">
            <div className="max-w-6xl mx-auto px-4">

                <div className="flex justify-between py-2">

                    <div className="md:hidden flex items-center">
                        <button className="outline-none mobile-menu-button" onClick={toggleMenu}>
                            <svg className=" w-6 h-6 text-gray-500 hover:text-green-500 "
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                            >
                                <path d="M4 6h16M4 12h16M4 18h16"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            {mobileMenu && <div className="mobile-menu flex flex-col">
                <Link href="/"><span className="cursor-pointer py-2 px-5 text-gray-900 font-semibold hover:text-cyan-500 transition duration-300">Order</span></Link>
                <Link href="/vehicle-report"><span className="cursor-pointer py-2 px-5 text-gray-900 font-semibold hover:text-cyan-500 transition duration-300">Bill</span></Link>
                <Link href="/pricing" > <span className="cursor-pointer py-2 px-5 text-gray-900 font-semibold hover:text-cyan-500 transition duration-300">Profile</span></Link >
            </div >}
        </nav >
    )
}

export default Navbar
