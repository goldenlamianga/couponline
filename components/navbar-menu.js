import { useState, React, useEffect } from 'react'
import { FaShoppingBasket, FaClipboardList } from 'react-icons/fa';
import { MdAccountCircle } from 'react-icons/md';
import Router from 'next/router'

function NavbarMenu() {
    const [onHoverMenuOrder, setOnHoverMenuOrder] = useState(false)
    const [onHoverMenuBill, setOnHoverMenuBill] = useState(false)
    const [onHoverMenuProfile, setOnHoverMenuProfile] = useState(false)
    var pathName = '';
    if (Router.router != null) {
        pathName = Router.router.pathname;
    }


    function onLoad() {
        console.log("NavbarMenu : " + pathName);
        if (pathName.includes('/order')) {
            setOnHoverMenuOrder(true);
            setOnHoverMenuBill(false);
            setOnHoverMenuProfile(false);
        } else if (pathName.includes('/bill')) {
            setOnHoverMenuOrder(false);
            setOnHoverMenuBill(true);
            setOnHoverMenuProfile(false);
        } else if (pathName.includes('/profile')) {
            setOnHoverMenuOrder(false);
            setOnHoverMenuBill(false);
            setOnHoverMenuProfile(true);
        }
    }

    useEffect(() => {
        onLoad();
    }, []);

    return (
        <section className="relative py-1 mb-4">
            <nav className="w-full fixed inset-x-0 bottom-0 w-100-px h-14 bg-white px-1 pt-0 shadow">
                <div className="mb-3 flex flex-row justify-evenly pt-1">
                    <button onClick={() => { Router.push('/order') }} className="w-full focus:text-white flex justify-center items-center place-items-center text-center pt-2 pb-1">
                        <div className='flex flex-col justify-center items-center  gap-1 font-semibold'>
                            <FaShoppingBasket size={18} className={`text-xs ${onHoverMenuOrder && 'text-red-600'} ${!onHoverMenuOrder && 'text-black'}`} />
                            <span className={`text-xs ${onHoverMenuOrder && 'text-red-600'} ${!onHoverMenuOrder && 'text-black'}`}>Order</span>
                        </div>
                    </button>
                    <button onClick={() => { Router.push('/bill') }} className="w-full focus:text-white flex justify-center items-center place-items-center text-center pt-2 pb-1">
                        <div className='flex flex-col justify-center items-center gap-1 font-semibold'>
                            <FaClipboardList size={18} className={`text-xs ${onHoverMenuBill && 'text-red-600'} ${!onHoverMenuBill && 'text-black'}`} />
                            <span className={`text-xs ${onHoverMenuBill && 'text-red-600'} ${!onHoverMenuBill && 'text-black'}`}>Bill</span>
                        </div>
                    </button>
                    <button onClick={() => { Router.push('/profile') }} className="w-full focus:text-white flex justify-center items-center place-items-center text-center pt-2 pb-1">
                        <div className='flex flex-col justify-center items-center  gap-1 font-semibold'>
                            <MdAccountCircle size={18} className={`text-xs ${onHoverMenuProfile && 'text-red-600'} ${!onHoverMenuProfile && 'text-black'}`} />
                            <span className={`text-xs ${onHoverMenuProfile && 'text-red-600'} ${!onHoverMenuProfile && 'text-black'}`}>Profile</span>
                        </div>
                    </button>

                </div>
                {/* <div>
                    <Modal
                        isOpen={modalIsOpen}
                        onAfterOpen={afterOpenModal}
                        onRequestClose={closeModal}
                        style={customStylesLogin}
                        contentLabel="Login">
                        <Login onClose={closeModal} />
                    </Modal>

                </div>
                <div>
                    <Modal
                        isOpen={modalAdsIsOpen}
                        onRequestClose={closeModalAds}
                        style={customStyles}
                        ariaHideApp={false}
                        contentLabel="Ads">
                        <div className='h-full w-full bg-gray-700'>
                            <AdsBanner onClose={closeModalAds} />
                        </div>
                    </Modal>
                </div> */}
            </nav>
        </section>
    )
}
export default NavbarMenu