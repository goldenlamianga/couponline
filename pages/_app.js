import { AnimateSharedLayout } from "framer-motion"
import { SessionProvider } from "next-auth/react"
import Head from 'next/head'
import { React } from 'react'
import 'tailwindcss/tailwind.css'
import Layout from '../components/layout'
import '../styles/faq.css'
import { MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';

function MyApp({ Component, pageProps: { session, ...pageProps }, ...appProps }) {
  // const handleScroll = () => {
  //   if (window.scrollY > 80) {
  //     document.querySelector(".body").className = "body lg:overflow-hidden body-fix overflow-x-scroll lg:overflow-x-hidden";
  //   } else {
  //     document.querySelector(".body").className = "body lg:overflow-hidden lg:overflow-x-hidden overflow-x-scroll";
  //   }
  // };

  // useEffect(() => {
  //   window.addEventListener("scroll", handleScroll);

  //   return () => {
  //     window.removeEventListener("scroll", handleScroll);
  //   };
  // }, [])

  // useEffect(() => {

  //   var tawk = new TawkTo("61f8a50bb9e4e21181bcd97b", "1fqpk9dv7")

  //   tawk.onStatusChange((status) => {
  //     // console.log(status)
  //   })

  // }, [])


  if ([`/report/[pid]`, `/manageorders`, `/sample-report-full`, `/admin-view-order`, `/admin-view-contact`, `/admin-view-order-complete`, `/admin`, `/admin-view-admin`, `/admin-view-customer`, `/admin-edit-profile`, `/cart`].includes(appProps.router.pathname)) {
    return (<SessionProvider session={session}><Component {...pageProps} /></SessionProvider>);
  }
  return (
    <MantineProvider>
      <ModalsProvider>
        <AnimateSharedLayout>
          <Layout>
            <div className="body">
              <Head>
                <title>Golden Lamian Coupon</title>
                <meta
                  name='description'
                  content='Take the guesswork out of your decision with trusted data source.'
                />
                <meta
                  name='keywords'
                  content='Our Brand Golden Lamian, Hey Kafe, Woy makaroni, Sozo dll.'
                />
                <link rel="icon" href="/favicon.png" />
              </Head>
              <Component {...pageProps} />
            </div>
          </Layout>
        </AnimateSharedLayout>
      </ModalsProvider>
    </MantineProvider>
  )
}

export default MyApp