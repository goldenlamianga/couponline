import { React, useState, useEffect } from 'react'
import { motion } from "framer-motion"
import 'react-circular-progressbar/dist/styles.css';
import CouponContainerDemo from '../components/coupon.container-demo';
import CouponContainerResponsive from '../components/coupon.container.responsive';
import ReactLoading from "react-loading"

function Coupon() {
    const [shownPage, setShownPage] = useState(true)

    useEffect(() => {
    }, [])

    if (shownPage) {
        return (
            // <div className="max-w-6xl mx-auto">
            <div>
                <main className="overflow-y-auto overflow-x-hidden
                w-full h-screen ">
                    <div>
                        {/* Body-Payment-Method */}
                        <motion.div transition={{ delay: 0.2, y: { type: "spring", stiffness: 40 } }} initial={{ y: 30, opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                            <div className="bg-seven-red w-full flex items-center justify-center py-10">
                                <img src="/assets/logo-golden.jpg" className='w-80' alt="Logo" />
                            </div>
                        </motion.div >
                        <motion.div transition={{ delay: 0.2, y: { type: "spring", stiffness: 40 } }} initial={{ y: 30, opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                            <CouponContainerResponsive />
                        </motion.div>
                    </div >
                </main>
            </div >
        )
    }
    return (
        <div className='flex justify-center items-center h-screen'>
            <ReactLoading type="bubbles" color="#4287f5" />
        </div>
    )
}

export default Coupon
