import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, orderBy, limit } from 'firebase/firestore'

const dbInstance = collection(database, 'promos')

export default async function handler(req, res) {
    try {
        var itemColl = []

        const q = query(dbInstance, where('shownads', '==', 1), orderBy("id"), limit(1));
        const querySnapshot = await getDocs(q).then((data) => {
            if (!data.empty) {
                data.docs.map((item) => {
                    itemColl.push({ ...item.data() })
                });
            }
        });

        res.status(200).json({ success: true, message: "", data: itemColl, error: null })
    } catch (error) {
        res.status(200).json({ success: false, message: "something went wrong", data: "", error: error })
    }
}