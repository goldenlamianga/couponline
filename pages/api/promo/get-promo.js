import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, orderBy } from 'firebase/firestore'

const dbInstance = collection(database, 'promos')

export default async function handler(req, res) {
    try {
        var itemColl = []
        // var SalesArea = res.body.SalesArea
        // res.status(200).json(SalesArea)

        const q = query(dbInstance, where('status', '==', 0), orderBy("id"));
        const querySnapshot = await getDocs(q).then((data) => {
            if (!data.empty) {
                data.docs.map((item) => {
                    itemColl.push({ ...item.data() })
                });
            }
        });

        res.status(200).json({ success: true, message: "", data: itemColl, error: null })
    } catch (error) {
        res.status(200).json({ success: false, message: "something went wrong", data: "", error: error })
    }
}