import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, orderBy } from 'firebase/firestore'
const dbInstance = collection(database, '/Brand/golden/direct_coupon_claim')

export default async function handler(req, res) {
    try {
        var itemColl = []
        var dataRaw = req.body
        var coupon_id = dataRaw.coupon_id
        var no_hp = dataRaw.no_hp
        var max_redem = dataRaw.max_redem
        // const q = query(dbInstance);
        const q = query(dbInstance,
            where('phone_number', '==', no_hp),
            where('id_coupon', '==', coupon_id)
        );
        const querySnapshot = await getDocs(q).then((data) => {
            if (data.size > max_redem) {
                res.status(200).json({ success: false, message: "Nomor Handphone sudah terdaftar, coupon max", data: [], error: null })
            }
        });

        res.status(200).json({ success: true, message: "", data: itemColl, error: null })
    } catch (error) {
        res.status(200).json({ success: false, message: "something went wrong", data: "", error: error })
    }
}