import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, FieldValue } from 'firebase/firestore'

const dbInstance = collection(database, '/Brand/golden/direct_coupon_claim')

export default async function handler(req, res) {
    var dataRaw = req.body
    var coupon_id = dataRaw.coupon_id
    var no_hp = dataRaw.no_hp
    var salesArea = dataRaw.SalesArea
    // res.status(200).json(dataRaw)
    // res.status(200).json({ id: coupon_id, nohp: no_hp, sales_area: salesArea })

    const modelCoupon = {
        "id_coupon": coupon_id,
        "SalesArea": salesArea,
        "phone_number": no_hp,
        "status": 0,
        "createdAt": new Date(),
        "createdBy": new Date(),
        "updatedAt": new Date(),
        "updatedBy": new Date()
    }
    // return res.status(200).json({ success: true, message: "Under Construction", error: null })
    try {
        const q = query(dbInstance);

        const sendData = await addDoc(dbInstance, modelCoupon).then((data) => {
            if (!data.empty) {
                res.status(200).json({ success: true, message: data.id, error: null })
            } else {
                res.status(200).json({ success: false, message: data, error: null })
            }
        });

    } catch (error) {
        res.status(500).json({ success: false, message: "something went wrong", error: error })
    }
}