import { app, database } from '/firebaseConfig'
import { doc, collection, query, where, addDoc, updateDoc, getDocs, FieldValue } from 'firebase/firestore'

export default async function handler(req, res) {
    const id = req.body.id;
    const status = req.body.status;
    const img = req.body.img;
    const updatedBy = req.body.updatedBy;
    const modelCoupon = {
        "status": status,
        "img": img,
        "updatedAt": new Date(),
        "updatedBy": updatedBy
    }
    // return res.status(200).json({ success: true, message: "Under Construction", error: modelCoupon })
    try {
        const dbInstance = doc(database, '/Brand/golden/direct_coupon', id)
        const sendData = await updateDoc(dbInstance, modelCoupon).then((data) => {
            res.status(200).json({ success: true, message: data, error: null })
        });
    } catch (error) {
        res.status(500).json({ success: false, message: "something went wrong", error: error })
    }
}