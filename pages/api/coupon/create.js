import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, FieldValue } from 'firebase/firestore'

const dbInstance = collection(database, '/Brand/golden/direct_coupon')

export default async function handler(req, res) {
    const dataBody = req.body;
    const id = dataBody.id;
    const salesArea = dataBody.SalesArea;
    const title = dataBody.title;
    const detail = dataBody.detail;
    const code = dataBody.code;
    const img = dataBody.img;
    const status = dataBody.status;
    const max_redem = dataBody.max_redem;
    const periode_start = dataBody.periode_start + "T00:00:01.811Z";
    const periode_end = dataBody.periode_end + "T16:59:59.811Z";
    const modelCoupon = {
        "id": id,
        "SalesArea": salesArea,
        "title": title,
        "detail": detail,
        "code": code,
        "img": img,
        "periode_start": new Date(periode_start),
        "periode_end": new Date(periode_end),
        "status": status,
        "max_redem": max_redem,
        "createdAt": new Date(),
        "createdBy": 'admin',
        "updatedAt": new Date(),
        "updatedBy": 'admin'
    }
    // return res.status(200).json({ success: true, message: "Under Construction, please enable this action api", data: modelCoupon, error: { periode_start, periode_end } })
    try {
        const q = query(dbInstance);

        const sendData = await addDoc(dbInstance, modelCoupon).then((data) => {
            if (!data.empty) {
                res.status(200).json({ success: true, message: data.id, data: modelCoupon, error: null })
            } else {
                res.status(200).json({ success: false, message: data, error: null })
            }
        });

    } catch (error) {
        res.status(500).json({ success: false, message: "something went wrong", error: error })
    }
}