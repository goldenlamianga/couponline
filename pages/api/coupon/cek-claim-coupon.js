import { app, database } from '/firebaseConfig'
import { collection, query, where, addDoc, getDocs, orderBy } from 'firebase/firestore'
const dbInstance = collection(database, '/Brand/golden/direct_coupon_claim')

export default async function handler(req, res) {
    try {
        var itemColl = []
        var dataRaw = req.body
        var coupon_id = dataRaw.coupon_id
        var no_hp = dataRaw.no_hp
        var max_redem = dataRaw.max_redem
        var periode_start = dataRaw.periode_start
        var periode_end = dataRaw.periode_end
        var data_size = 0
        // res.status(200).json({ id: coupon_id, nohp: no_hp, maxredem: max_redem, periodestart: periode_start, periodeend: periode_end })
        const q = query(dbInstance,
            where('phone_number', '==', no_hp),
            where('id_coupon', '==', coupon_id)
        );
        const querySnapshot = await getDocs(q).then((data) => {
            if (!data.empty) {
                data.docs.map((item) => {
                    itemColl.push({ ...item.data(), data: data.size })
                });
                data_size = data.size
            }
        });
        res.status(200).json({ success: true, message: "", data: data_size, error: null })
    } catch (error) {
        res.status(200).json({ success: false, message: "something went wrong", data: "", error: error })
    }
}