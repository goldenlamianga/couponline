import React from 'react'
import { BiLeftArrowAlt } from 'react-icons/bi';
import Router from 'next/router'
import { FaArrowRight, FaStarHalfAlt, FaStar } from "react-icons/fa"
import { CircularProgressbar, CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

function Blank() {
    return (
        <div className="max-w-6xl mx-auto">
            <header>
                <div className="border-gray-100 bg-white border-b-2 py-2">
                    <div className="flex justify-start py-2 px-3">
                        <div className="pr-4 flex items-center">
                            <button onClick={() => Router.back()} className="outline-none mobile-menu-button">
                                <BiLeftArrowAlt size={30} className='text-black' />
                            </button>
                        </div>
                        <div className="w-3/4 pl-2 text-left text-lg align-center text-black">

                        </div>
                    </div>
                </div>
            </header>

            <div className='hidden flex justify-between pl-4 pr-1 pt-2 border-b'>
                <div className="w-1/2 flex justify-center items-center">
                    <div className="relative z-0">
                        <CircularProgressbarWithChildren
                            value={30}
                            minValue={0}
                            maxValue={100}
                            styles={buildStyles({
                                pathColor: "rgba(62, 152, 199)",
                                trailColor: "#eee",
                                strokeLinecap: "rounded",
                                transformOrigin: "center center",
                                transform: "rotate(1.25turn)",
                                rotation: 1 / 2 + 1 / 6.38,
                                transition: "stroke-dashoffset 0.5s ease 1s"
                            })}
                        >
                            {/* Foreground path */}
                            <CircularProgressbar
                                value={30}
                                styles={buildStyles({
                                    pathColor: "#fff",
                                    trailColor: "transparent",
                                    strokeLinecap: "butt",
                                    rotation: 1 / 2.8,
                                })}
                            />
                        </CircularProgressbarWithChildren>
                        <div className="absolute inset-0 flex justify-center items-center z-10">
                            <div className='grid grid-cols-1 items-center justify-items-center py-1'>
                                <FaStar className='text-seven-gold1' size={60} />
                                <div className='text-xl pb-2'>
                                    <label>4/8</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='flex-1 grid grid-cols-1 items-center justify-between place-items-center py-5 ml-2'>
                    <strong className='text-sm'>REWARDS</strong>
                    <strong className='text-sm text-seven-gold2'>Golden Level 1</strong>
                    {/* <CircularProgressbar className='rotate-0' strokeWidth="5" circleRatio="10" value={value} minValue={0} maxValue={2} text={`${value * 100}%`} /> */}
                    {/*  */}
                    <div className='text-2xl pb-2'>
                        <strong>1</strong>
                    </div>
                    <label className='text-xs'>Star until</label>
                    <label className='text-sm'>Next Reward</label>
                </div>
            </div>
            <div className='hidden flex justify-between pl-4 pr-1 pt-2 border-b'>
                <div className="w-1/2 flex justify-center items-center">
                    <div className="relative z-0">
                        <CircularProgressbarWithChildren
                            value={100}
                            styles={
                                buildStyles({
                                    pathColor: "#fff",
                                    trailColor: "#fff",
                                    strokeLinecap: "butt",
                                    trail: {
                                        // Trail color
                                        stroke: "#FFFFFF",
                                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                        strokeLinecap: "butt",
                                        // Rotate the trail
                                        transform: "rotate(0.25turn)",
                                        transformOrigin: "center center",
                                        transition: "stroke-dashoffset 0.5s ease 1s"
                                    }
                                })}
                        >
                            {/* Foreground path */}
                            <CircularProgressbar
                                value={50}
                                strokeWidth={5}
                                styles={buildStyles({
                                    trailColor: "#FCF",
                                    stroke: "#FCF",
                                    pathColor: "rgba(62, 152, 199)",
                                    rotation: 1 / 2 + 1 / 6.7,
                                    strokeLinecap: "rounded"
                                })}
                            />
                        </CircularProgressbarWithChildren>
                        <div className="absolute inset-0 flex justify-center items-center z-10">
                            <div className='grid grid-cols-1 items-center justify-items-center py-1'>
                                <FaStar className='text-seven-gold1' size={60} />
                                <div className='text-xl pb-2'>
                                    <label>4/8</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='flex-1 grid grid-cols-1 items-center justify-between place-items-center py-5 ml-2'>
                    <strong className='text-sm'>REWARDS</strong>
                    <strong className='text-sm text-seven-gold2'>Golden Level 1</strong>
                    {/* <CircularProgressbar className='rotate-0' strokeWidth="5" circleRatio="10" value={value} minValue={0} maxValue={2} text={`${value * 100}%`} /> */}
                    {/*  */}
                    <div className='text-2xl pb-2'>
                        <strong>1</strong>
                    </div>
                    <label className='text-xs'>Star until</label>
                    <label className='text-sm'>Next Reward</label>
                </div>
            </div>


            <div className='hidden flex-1 grid grid-cols-1 items-center justify-items-center pt-2'>
                <CircularProgressbarWithChildren value={30} styles={{
                    // Customize the root svg element
                    root: {},
                    // Customize the path, i.e. the "completed progress"
                    path: {
                        strokeWidth: 2,
                        // Path color
                        stroke: `rgba(62, 152, 199)`,
                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                        strokeLinecap: 'round',
                        // Customize transition animation
                        transition: 'stroke-dashoffset 0.5s ease 0s',
                        // Rotate the path
                        transform: 'rotate(-0.352turn)',
                        transformOrigin: 'center center',
                    },
                    // Customize the circle behind the path, i.e. the "total progress"
                    trail: {
                        // Trail color
                        stroke: '#ffffff',
                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                        strokeLinecap: 'butt',
                        // Rotate the trail
                        transform: 'rotate(0.25turn)',
                        transformOrigin: 'center center',
                    }
                }}>
                    {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                    <FaStar className='text-seven-gold1' size={60} />
                    <div className='text-xl pb-2'>
                        <label>4/8</label>
                    </div>
                </CircularProgressbarWithChildren>
                {/* <label className='text-sm'>5/12</label> */}
            </div>

            <section className="w-full h-screen">
                <img src="https://www.kindpng.com/picc/m/444-4446574_under-construction-404-page-under-construction-illustrator-illustration.png" className="object-contain w-full h-full" alt="Image alt text" />
            </section>
        </div>
    )
}

export default Blank
