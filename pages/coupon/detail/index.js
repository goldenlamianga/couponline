import { React, useState, useEffect } from 'react'
import Router from 'next/router'
import { motion } from "framer-motion"
import { BiLeftArrowAlt } from 'react-icons/bi';
import getTmpSession from '../../../middleware/coupon/getTmpSession';
import setTmpSession from '../../../middleware/coupon/setTmpSession';
import { Button } from '@mui/material';

function Promo() {
    const [shownPage, setShownPage] = useState(false)
    const [title, setTitle] = useState("")
    const [detail, setDetail] = useState("")
    const [img, setImg] = useState("/assets/gl-logo.png")

    const handleClick = (e) => {
        e.preventDefault()
        setTmpSession(null)
        Router.back()
    }

    async function getPromo() {
        var dataPromo = await getTmpSession()
        console.log(dataPromo)
        if (dataPromo != null) {
            setTitle(dataPromo.title)
            setDetail(dataPromo.detail)
            setImg(dataPromo.img)
        } else {
            handleBack("getPromo")
        }
    }

    const redemCoupon = async () => {
        console.log('Lol')
    }

    const cekLogin = async () => {
        const rst = await userCekIsLoggedIn()
        if (!rst) { handleBack("cekLogin") }
    }

    function handleBack(from) {
        console.log("=========PROMO-DETAIL-DONE===========>" + from);
        return Router.push('/login')
    }

    useEffect(() => {
        console.log("============PROMO-DETAIL=============>")
        getPromo()
        let search = window.location.search;
        let params = new URLSearchParams(search);
        let v = params.get('id');
        console.log('id promo : ' + v)
    }, [])


    const variants = {
        /** this is the "visible" key and it's correlating styles **/
        visible: { opacity: 1, y: 0, display: "block" },
        /** this is the "hidden" key and it's correlating styles **/
        hidden: { opacity: 0, y: -10, display: "none" }
    };


    return (
        // <div className="max-w-6xl mx-auto px-0">
        <div className="">
            {/* <Navbar /> */}
            <header>
                <div className="border-gray-100 border-b-2 py-2">
                    <div className="flex justify-start py-2 px-3">
                        <div className="pr-4 flex items-center">
                            <button onClick={handleClick} className="outline-none mobile-menu-button">
                                <BiLeftArrowAlt size={30} className='text-black' />
                            </button>
                        </div>
                        <div className="w-3/4 text-normal align-left flex items-center justify-start text-black">
                            Detail
                        </div>
                    </div>
                </div>
            </header>
            <div className='max-w-4xl lg:px-8 h-full pb-10'>
                <div className={`w-full lg:w-1/1 pr-0 lg:px-10 pb-10'}`}>
                    {/* Header Content */}
                    <motion.div transition={{ delay: 0.2, y: { type: "spring", stiffness: 40 } }} initial={{ y: 30, opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                        <div className='relative mb-5'>
                            <div className="relative overflow-hidden cursor-pointer py-2">
                                <img className="
                                lg:w-full md:w-full sm:w-full  
                                lg:object-contain
                                w-10
                                h-96
                                object-full
                                " src={img} alt="img label" />
                            </div>
                        </div>
                    </motion.div>
                    <motion.div transition={{ delay: 0.2, y: { type: "spring", stiffness: 40 } }} initial={{ y: 30, opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                        <div className='relative m-5'>
                            <ol>
                                <li className='font-semibold'>{title}</li>
                            </ol>
                            <p>&nbsp;</p>
                            <p className='text-sm text-justify'>{detail}</p>
                            <p>&nbsp;</p>
                        </div>
                    </motion.div>
                    <motion.div transition={{ delay: 0.2, y: { type: "spring", stiffness: 40 } }} initial={{ y: 30, opacity: 0 }} animate={{ y: 0, opacity: 1 }}>
                        <div className='relative m-5 flex justify-center'>
                            <Button variant="contained" component="span" onClick={redemCoupon}>
                                Redeem Now
                            </Button>
                        </div>
                    </motion.div>
                </div>
            </div>
        </div>

    )
}

export default Promo
