import { useState, useEffect, React } from 'react'
import Header from '../../../components/admin/header'
import Sidebar from '../../../components/admin/sidebar'
import userAdminLoginGet from '../../../middleware/admin/userAdminLoginGet'
import Router from 'next/router'
import cekAdminSession from '../../../middleware/admin/cekAdminSession'
import MUIDataTable from "mui-datatables";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Create';
import IconButton from '@mui/material/IconButton';
import getConvertDateTime from '../../../middleware/getConvertDateTime';
import getConvertDate from '../../../middleware/getConvertDate';
import {  database } from '/firebaseConfig'
import { doc, collection, query, where, onSnapshot, setDoc, addDoc, getDoc, getDocs, deleteDoc, updateDoc, orderBy, limit, FieldValue } from 'firebase/firestore';
import { getStorage, ref, uploadBytes, uploadBytesResumable, getDownloadURL, deleteObject } from "firebase/storage";
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import { v4 as uuidv4 } from 'uuid'

// Mantine Form & Modal
import { TextInput, InputWrapper, NumberInput, Textarea, Text, Select, Image, Space, Group, MultiSelect, Switch, Modal } from '@mantine/core';

// Croping Image
// Import React FilePond
import { FilePond, File, registerPlugin } from 'react-filepond'
// Import FilePond styles
import 'filepond/dist/filepond.min.css'

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
// `npm i filepond-plugin-image-preview filepond-plugin-image-exif-orientation --save`
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation'
import FilePondPluginImagePreview from 'filepond-plugin-image-preview'
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css'
// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview)

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
//     <button className="example-custom-input w-full text-left" onClick={onClick} ref={ref}>
//         {value}
//     </button>
// ));

function CouponPage() {
    const [userLoginData, setUserLoginData] = useState([])
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [loading, setLoading] = useState(true)

    const [filePondRef, setFilePond] = useState(null)
    // const filePondRef = useRef("123")
    const [files, setFiles] = useState([])
    const [filesOption, setFilesOption] = useState([])
    const [periodeStart, setPeriodeStart] = useState(
        // new Date(2021, 11, 1),
        new Date()
    );
    const [periodeEnd, setPeriodeEnd] = useState(new Date());

    const [isOpenDelete, setOpenedDelete] = useState(false)

    const [dataCoupons, setDataCoupons] = useState([])
    const [modelCoupons, setModelCoupons] = useState({
        "id": 1,
        "SalesArea": "",
        "title": "",
        "detail": "",
        "brand": "gl",
        "code": "",
        "img": "",
        "periode_start": new Date(),
        "periode_end": new Date(),
        "status": 1,
        "max_redem": 0,
        "createdAt": new Date(),
        "createdBy": '',
        "updatedAt": new Date(),
        "updatedBy": '',
        "role": "admin"
    })



    // ROW ACTION
    const [isOpen, setIsOpen] = useState(false)
    const [isOpenEdit, setIsOpenEdit] = useState(false)
    const [isOpenEditStatus, setOpenedStatus] = useState(false)
    const [dataSelected, setDataSelected] = useState([])
    const [idxSelected, setIdxSelected] = useState("")

    //FIREBASE
    let unsubscribe;
    var limitLeng = 10;

    const cekLogin = async () => {
        const rst = await userAdminLoginGet()
        // console.log("cekLogin : ", rst)
        if (rst == null) {
            setIsLoggedIn(false)
            return Router.push('/admin/login')
        }
        setUserLoginData(rst)
        setIsLoggedIn(true)
        return rst
    }

    const openKonfirmDelete = async (data, idx) => {
        // console.log('OPEN DIALOG CANCEL ORDER : ', idx, data)
        setDataSelected(data)
        setIdxSelected(idx)
        setOpenedDelete(true)
    }
    const closeModalDelete = () => {
        setOpenedDelete(false);
    }

    const closeModal = () => {
        resetFieldModal();
        setIsOpen(false);
        setIsOpenEdit(false);
        setOpenedStatus(false);
    }
    const resetFieldModal = () => {
        cleanImgFile()
        setModelCoupons({
            "id": 1,
            "SalesArea": "",
            "title": "",
            "detail": "",
            "detail": "gl",
            "code": "",
            "img": "",
            "periode_start": new Date(),
            "periode_end": new Date(),
            "status": 1,
            "max_redem": 0,
            "createdAt": new Date(),
            "createdBy": '',
            "updatedAt": new Date(),
            "updatedBy": '',
            "role": "admin"
        });
        setPeriodeStart(new Date())
        setPeriodeEnd(new Date())
    }
    const cleanImgFile = () => {
        try {
            files = [];
            setFiles([]);
            // filePondRef?.current?.removeFile(files);
            filePondRef.removeFile()
        } catch (error) {
            console.log('->Error cleanImgFile : ', error)
        }

    }

    const openAdd = async () => {
        // console.log('Add : ', modelCoupons, userLoginData)
        setModelCoupons({ ...modelCoupons, createdBy: userLoginData.nama });
        files = [];
        setFiles([]);
        setFilesOption(files);
        setIsOpen(true)
        return
    }

    const openKonfirmEdit = async (data, idx) => {
        // console.log('Edit Coupon : ', idx, data, files)
        setDataSelected(data)
        setIdxSelected(idx)

        modelCoupons.idfs = data.idfs;
        modelCoupons.id = data.id ?? 1;
        modelCoupons.title = data.title;
        modelCoupons.brand = data.brand ?? "gl";
        modelCoupons.detail = data.detail ?? "";
        modelCoupons.img = data.img ?? "";
        modelCoupons.status = data.status ?? false;
        modelCoupons.periode_start = (data.periode_start != "") ? new Date(data.periode_start) : new Date();
        modelCoupons.periode_end = (data.periode_end != "") ? new Date(data.periode_end) : new Date();
        modelCoupons.createdAt = (data.createdAt != "") ? new Date(data.createdAt) : new Date();
        modelCoupons.createdBy = data.createdBy ?? "admin";
        modelCoupons.updatedBy = userLoginData.nama ?? "admin";
        setModelCoupons({ ...modelCoupons, id: data.id ?? 1 });

        if (data.img == "") {
            files = [];
            setFiles([]);
            setFilesOption(files);
            // console.log('Edit Clean Image : ', filePondRef, files);
        } else {
            setFilesOption([{
                source: data.img,
                options: {
                    type: 'local'
                }
            }])
        }


        setTimeout(() => {
            setIsOpenEdit(true)
        }, 200);

        return
    }

    const columns = [
        {
            name: "idfs",
            label: "ID",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-center text-sm border-r-2 h-10' key={0} onClick={() => updateDirection(0)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRender: (value, tableMeta) => {
                    return <div className="flex justify-center">
                        {(value).substr(0, 10) + "..."}
                    </div>;
                }
            }
        },
        {
            name: "createdAt",
            label: "Date Create",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-left text-sm pl-5 border-r-2 h-10 ' key={1} onClick={() => updateDirection(1)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRender: (value, tableMeta) => {
                    return <div className="flex justify-center">
                        {getConvertDateTime(value)}
                    </div>;
                }
            }
        },
        {
            name: "title",
            label: "Title",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-left text-sm pl-5 border-r-2 h-10' key={2} onClick={() => updateDirection(2)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
            }
        },
        {
            name: "periode_start",
            label: "Periode Mulai",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-left text-sm pl-5 border-r-2 h-10' key={3} onClick={() => updateDirection(3)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRender: (value, tableMeta) => {
                    return <div className="flex justify-center">
                        {getConvertDate(value)}
                    </div>;
                }
            }
        },
        {
            name: "periode_end",
            label: "Periode Akhir",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-left text-sm pl-5 border-r-2 h-10' key={4} onClick={() => updateDirection(4)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRender: (value, tableMeta) => {
                    return <div className="flex justify-center">
                        {getConvertDate(value)}
                    </div>;
                }
            }
        },
        {
            name: "img",
            label: "Image",
            options: {
                filter: false,
                sort: true,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-left text-sm pl-5 border-r-2' key={5} onClick={() => updateDirection(5)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRenderLite: (dataIndex, rowIndex) => {
                    return (
                        <Image
                            width={60}
                            height={60}
                            radius="sm"
                            src={(dataCoupons[dataIndex].img != "") ? dataCoupons[dataIndex].img : "/noimages.png"} />
                    );
                },
            }
        },
        {
            name: "status",
            label: "Status",
            options: {
                filter: true,
                sort: false,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-center text-sm border-r-2 h-10' key={6} onClick={() => updateDirection(6)} style={{ cursor: 'pointer' }}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRender: (value, tableMeta) => {
                    return <div className="flex justify-center">
                        <Switch name="asguide" onLabel="ON" offLabel="OFF" onChange={(rst) => { onChangeStatusLive(rst.currentTarget.checked, tableMeta.rowData) }} checked={!value}></Switch>
                    </div>;
                }
            },
        },
        {
            name: "action",
            label: "Action",
            options: {
                filter: false,
                sort: false,
                customHeadRender: (columnMeta, updateDirection) => (
                    <th className='bg-gray-100 font-semibold text-sm h-10' key={7}>
                        {columnMeta.label}
                    </th>
                ),
                customBodyRenderLite: (dataIndex, rowIndex) => {
                    return (
                        <div className='flex justify-evenly'>
                            <IconButton aria-label="delete" sx={{ fontSize: 20 }} onClick={() => { openKonfirmEdit(dataCoupons[dataIndex], dataIndex) }}>
                                <EditIcon fontSize="inherit" />
                            </IconButton>
                            <IconButton aria-label="delete" sx={{ fontSize: 20 }} onClick={() => { openKonfirmDelete(dataCoupons[dataIndex], dataIndex) }}>
                                <DeleteIcon fontSize="inherit" />
                            </IconButton>
                        </div>
                    );
                },

            }
        },
    ];

    const options = {
        tableId: 'Coupons',
        search: true,
        fixedHeader: true,
        selectableRows: 'none',
        filterType: 'dropdown',
        responsive: 'standard',
        elevation: 1    //remove shadow/flat ui
    };

    const getDataCoupons = async () => {
        const dbInstance = collection(database, 'Brand/golden/direct_coupon')
        const q = query(dbInstance,
            orderBy("createdAt", "desc")
        );
        unsubscribe = onSnapshot(q,
            { includeMetadataChanges: true },
            (querySnapshot) => {
                try {
                    querySnapshot.docChanges().forEach((change) => {
                        const dataCouponsTmp = [];
                        // console.log("=======docChanges : ", change.type)
                        if (change.type === "added") {
                            querySnapshot.forEach((doc) => {
                                var periode_start = doc.data().periode_start?.toDate();
                                var periode_end = doc.data().periode_end?.toDate();
                                var createat = doc.data().createdAt?.toDate();
                                var updatedat = doc.data().createdAt?.toDate();
                                const modelTransaksi = {
                                    "idfs": doc.id,
                                    "id": doc.data().id,
                                    "SalesArea": doc.data().SalesArea ?? "",
                                    "title": doc.data().title ?? "",
                                    "detail": doc.data().detail ?? "",
                                    "brand": doc.data().brand ?? "gl",
                                    "code": doc.data().code ?? "",
                                    "img": doc.data().img ?? "",
                                    "periode_start": periode_start ?? "",
                                    "periode_end": periode_end ?? "",
                                    "status": doc.data().status ?? 1,
                                    "max_redem": doc.data().max_redem ?? 0,
                                    "createdAt": createat ?? "",
                                    "createdBy": doc.data().createdBy ?? "",
                                    "updatedAt": updatedat ?? "",
                                    "updatedBy": doc.data().createdBy ?? ""
                                }
                                dataCouponsTmp.push(modelTransaksi);
                            });
                            setDataCoupons(dataCouponsTmp)
                        }
                        if (change.type === "modified") {
                            querySnapshot.forEach((doc) => {
                                var periode_start = doc.data().periode_start?.toDate();
                                var periode_end = doc.data().periode_end?.toDate();
                                var createat = doc.data().createdAt?.toDate();
                                var updatedat = doc.data().createdAt?.toDate();
                                const modelTransaksi = {
                                    "idfs": doc.id,
                                    "id": doc.data().id,
                                    "SalesArea": doc.data().SalesArea ?? "",
                                    "title": doc.data().title ?? "",
                                    "detail": doc.data().detail ?? "",
                                    "brand": doc.data().brand ?? "gl",
                                    "code": doc.data().code ?? "",
                                    "img": doc.data().img ?? "",
                                    "periode_start": periode_start ?? "",
                                    "periode_end": periode_end ?? "",
                                    "status": doc.data().status ?? 1,
                                    "max_redem": doc.data().max_redem ?? 0,
                                    "createdAt": createat ?? "",
                                    "createdBy": doc.data().createdBy ?? "",
                                    "updatedAt": updatedat ?? "",
                                    "updatedBy": doc.data().createdBy ?? ""
                                }
                                dataCouponsTmp.push(modelTransaksi);
                            });
                            setDataCoupons(dataCouponsTmp)
                        }
                        if (change.type === "removed") {
                            querySnapshot.forEach((doc) => {
                                var periode_start = doc.data().periode_start?.toDate();
                                var periode_end = doc.data().periode_end?.toDate();
                                var createat = doc.data().createdAt?.toDate();
                                var updatedat = doc.data().createdAt?.toDate();
                                const modelTransaksi = {
                                    "idfs": doc.id,
                                    "id": doc.data().id,
                                    "SalesArea": doc.data().SalesArea ?? "",
                                    "title": doc.data().title ?? "",
                                    "detail": doc.data().detail ?? "",
                                    "brand": doc.data().brand ?? "gl",
                                    "code": doc.data().code ?? "",
                                    "img": doc.data().img ?? "",
                                    "periode_start": periode_start ?? "",
                                    "periode_end": periode_end ?? "",
                                    "status": doc.data().status ?? 1,
                                    "max_redem": doc.data().max_redem ?? 0,
                                    "createdAt": createat ?? "",
                                    "createdBy": doc.data().createdBy ?? "",
                                    "updatedAt": updatedat ?? "",
                                    "updatedBy": doc.data().createdBy ?? ""
                                }
                                dataCouponsTmp.push(modelTransaksi);
                            });
                            setDataCoupons(dataCouponsTmp)
                        }
                    });
                    setLoading(false)
                } catch (error) {
                    try {
                        unsubscribe()
                    } catch (error) { }
                    setLoading(false);
                    console.log("->CatchErrorFirebase : ", error);
                }
            },
            (error) => {
                try {
                    unsubscribe()
                } catch (error) { }
                setLoading(false);
                console.log("->errorFirebase : ", error);
            }
        );
    }

    const onChangeTitle = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, title: dataSelected });
    }
    const onChangeDetail = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, detail: dataSelected });
    }

    const onChangeBrand = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, brand: dataSelected });
    }

    const onChangeStatus = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, status: (dataSelected ? 0 : 1) });
    }
    const onChangePeriodeStart = async (dataSelected) => {
        setPeriodeStart(dataSelected);
        setModelCoupons({ ...modelCoupons, periode_start: dataSelected });
    }
    const onChangePeriodeEnd = async (dataSelected) => {
        setPeriodeEnd(dataSelected);
        setModelCoupons({ ...modelCoupons, periode_end: dataSelected });
    }
    const onChangeShortId = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, id: dataSelected });
    }
    const onChangeMaxRedeem = async (dataSelected) => {
        setModelCoupons({ ...modelCoupons, max_redem: dataSelected });
    }
    const onChangeStatusLive = async (dataSelected, data) => {
        console.log("Change Status Live : ", dataSelected)
        setDataSelected(data)

        setModelCoupons({
            "idfs": data[0],
            "status": (dataSelected ? 0 : 1),
            "updatedAt": new Date(),
            "updatedBy": userLoginData.nama ?? "admin",
            "role": "admin",
        })

        setTimeout(() => {
            setOpenedStatus(true)
        }, 200);
    }

    const saveCoupon = async () => {
        // console.log("CEK SAVE NEW ", modelCoupons, userLoginData)
        if (modelCoupons.title == "") {
            alert("Harap isi title.")
            return
        }
        setModelCoupons({ ...modelCoupons, createdAt: new Date(), updatedAt: new Date() });
        // console.log("CEK DATA COUPON : ", modelCoupons)
        simpanDataStore()
        // 1. Save data
        // 2. get id
        // 3. Upload img by id
        // 4. update data by id

    }

    const simpanDataStore = async () => {
        // console.log("STORE NEW DATA : ", modelCoupons);
        try {
            const q = query(dbInstance);
            const dbInstance = collection(database, '/Brand/golden/direct_coupon')
            const sendData = await addDoc(dbInstance, modelCoupons).then(async (data) => {
                if (files.length > 0) {
                    const uploadImg = await handleUpload(data.id);
                    // console.log("Url img : ", modelCoupons, uploadImg)
                    if (uploadImg == false) {
                        closeModal();
                        setTimeout(() => {
                            alert("Data berhasil disimpan, Upload image gagal, coba periksa kembali lagi.")
                        }, 200);
                    } else {
                        const update_url_img = await updateUrlImg(data.id, uploadImg);
                        if (update_url_img == false) {
                            closeModal();
                            setTimeout(() => {
                                alert("Data berhasil disimpan, Update image url gagal, coba periksa kembali lagi.")
                            }, 200);
                        } else {
                            closeModal();
                            setTimeout(() => {
                                alert("Berhasil tambah data coupon.")
                            }, 200);
                        }
                    }
                } else {
                    closeModal();
                    setTimeout(() => {
                        alert("Berhasil tambah data coupon.")
                    }, 200);
                }
            });
        } catch (error) {
            console.log("->error simpanDataStore : ", error)
            setTimeout(() => {
                alert("Gagal tambah data coupon.")
            }, 200);
        }

    }

    const handleUpload = async (idCoupon) => {
        try {
            if (files.length > 0) {
                const uuid = uuidv4();
                var metadata = {
                    contentType: 'image/jpeg',
                    metadata: {
                        firebaseStorageDownloadTokens: uuid
                    }
                };
                var fileNames = "/brand/Golden_Lamian/direct_coupon";
                fileNames = fileNames + "/" + idCoupon;
                // console.log("PATH IMG : ", fileNames)
                //jika nama file sama akan replace / overwrite
                const storage = getStorage();
                const imagesRef = ref(storage, fileNames);
                const uploadTask = await uploadBytes(imagesRef, files[0].file, metadata).then(async (snapshot) => {
                    const getUrl = await getDownloadURL(snapshot.ref).then((downloadURL) => {
                        // console.log("getDownloadURL : ", downloadURL)
                        modelCoupons.img = downloadURL
                        setModelCoupons({ ...modelCoupons, img: downloadURL });
                        return downloadURL
                    });
                    return getUrl
                });
                return uploadTask
            } else {
                // console.log("fileImg upload img : ", files)
                return false
            }
        } catch (error) {
            console.log("error catch upload img : ", error)
            return false
        }
    }
    const updateUrlImg = async (id, imgUrl) => {
        // console.log("EDIT URL IMG : ", modelCoupons, id, imgUrl);
        try {
            const sendData = await updateDoc(doc(database, "Brand/golden/direct_coupon", id.toString()), { img: imgUrl }).then((rst) => {
                closeModal();
                return true
            }).catch((err) => {
                console.log("->error updateDoc updateUrlImg : ", err)
                closeModal();
                setTimeout(() => {
                    alert("Gagal update data, coba kembali lagi.")
                }, 200);
                return false;
            })
        } catch (error) {
            console.log("->error updateUrlImg ", error);
            closeModal();
            setTimeout(() => {
                alert("Gagal update data, coba kembali lagi.")
            }, 200);
            return false;
        }
    }

    const saveCouponEdit = async () => {
        // console.log("CEK EDIT ", modelCoupons, userLoginData, filePondRef, files)
        if (modelCoupons.title == "") {
            setTimeout(() => {
                alert("Harap isi title")
            }, 200);
            return
        }
        if (modelCoupons.idfs == null) {
            closeModal();
            setTimeout(() => {
                alert("Terjadi kendala, coba kembali lagi.")
            }, 200);
            return
        } else if (modelCoupons.idfs == "") {
            closeModal();
            setTimeout(() => {
                alert("Terjadi kendala, coba kembali lagi.")
            }, 200);
            return
        }
        //upload replace img jika ada
        if (files.length > 0) {
            console.log("Upload/Replace IMG");
            const uploadImg = await handleUpload(modelCoupons.idfs)
            console.log("Url img : ", modelCoupons, uploadImg)
        } else {
            // remove img if already existing
            if (modelCoupons.img != "") {
                console.log("REMOVE IMG");
                const removeImgs = await removeImg(modelCoupons.idfs)
                if (removeImg) {
                    modelCoupons.img = "";
                    setModelCoupons({ ...modelCoupons, img: "" });
                }
            }
        }
        simpanDataEditStore()
        // 1. upload replace img, update url img OR remove img if already existing, update url img
        // 2. update data edit
    }
    const simpanDataEditStore = async () => {
        // console.log("EDIT COUPON : ", modelCoupons);
        try {
            const sendData = await updateDoc(doc(database, "/Brand/golden/direct_coupon", modelCoupons.idfs.toString()), modelCoupons).then((rst) => {
                closeModal();
                setTimeout(() => {
                    alert("Berhasil update coupon.")
                }, 200);
                return true
            }).catch((err) => {
                console.log("->error updateDoc : ", err)
                closeModal();
                setTimeout(() => {
                    alert("Gagal update coupon, coba kembali lagi.")
                }, 200);
                return;
            })
        } catch (error) {
            console.log("->error simpanDataEditStore ", error);
            closeModal();
            setTimeout(() => {
                alert("Gagal update coupon, coba kembali lagi.")
            }, 200);
            return;
        }
    }
    const simpanDataEditStatus = async () => {
        // console.log("EDIT STATUS LIVE : ", modelCoupons);
        try {
            if (modelCoupons.idfs != null) {
                if (modelCoupons.idfs != "") {
                    const sendData = await updateDoc(doc(database, "Brand/golden/direct_coupon", modelCoupons.idfs.toString()), modelCoupons).then((rst) => {
                        closeModal();
                        return true
                    }).catch((err) => {
                        console.log("->error updateDoc simpanDataEditStatus : ", err)
                        closeModal();
                        setTimeout(() => {
                            alert("Gagal update data, coba kembali lagi.")
                        }, 200);
                        return;
                    })
                } else {
                    console.log("->error modelCoupons.idfs empty ")
                    closeModal();
                    setTimeout(() => {
                        alert("Gagal update data, coba kembali lagi.")
                    }, 200);
                    return;
                }
            } else {
                console.log("->error modelCoupons.idfs null ")
                closeModal();
                setTimeout(() => {
                    alert("Gagal update data, coba kembali lagi.")
                }, 200);
                return;
            }
        } catch (error) {
            console.log("->error simpanDataEditStatus ", error);
            closeModal();
            setTimeout(() => {
                alert("Gagal update data, coba kembali lagi.")
            }, 200);
            return;
        }
    }

    const deleteCoupon = async () => {
        closeModalDelete()
        // console.log("deleteMenu Selected data : ", dataSelected, idxSelected)
        try {
            if (dataSelected.idfs == null) {
                setTimeout(() => {
                    return alert("Terjadi kendala kesalahan data, coba kembali lagi")
                }, 200);
            } else if (dataSelected.idfs == "") {
                setTimeout(() => {
                    return alert("Terjadi kendala kesalahan data, coba kembali lagi.")
                }, 200);
            }
            await deleteDoc(doc(database, "Brand/golden/direct_coupon", dataSelected.idfs.toString()));
            const imgRemove = await removeImg(dataSelected.idfs)
            if (!imgRemove) {
                console.log("Berhasil hapus data, Gagal Hapus Img coupon.")
            }
            setTimeout(() => {
                return alert("Berhasil hapus data coupon.")
            }, 200);
        } catch (error) {
            console.log("->Error deleteMenu : ", error)
            setTimeout(() => {
                return alert("Terjadi kendala, coba kembali lagi")
            }, 200);
        }
    }
    const removeImg = async (idCoupon) => {
        try {
            const storage = getStorage();
            const imagesRef = ref(storage, "/brand/Golden_Lamian/direct_coupon/" + idCoupon + "");
            return deleteObject(imagesRef).then(() => {
                console.log("Remove Img Success")
                return true
            }).catch((error) => {
                console.log("Error Remove Img ", error)
                return false
            });
        } catch (error) {
            console.log("Error removeImg ", error)
            return false
        }
    }

    useEffect(() => {
        cekAdminSession()
        cekLogin()
        getDataCoupons()
    }, []);

    useEffect(() => {
    }, [dataSelected, idxSelected, isOpen, userLoginData, modelCoupons]);

    useEffect(() => {
        //clean page existing
        return () => {
            try {
                console.log("Clean page COUPON MENU =>")
                unsubscribe();
            } catch (error) { }
        }
    }, []);

    if ((isLoggedIn)) {
        return (
            <div className='flex justify-start space-x-0'>
                {/* #MODAL ADD COUPONS_MENU  */}
                <Modal
                    centered
                    opened={isOpen}
                    onClose={() => closeModal()}
                    overflow="inside"
                >
                    <div>
                        <FilePond
                            ref={(ref) => { setFilePond(ref) }}
                            files={files}
                            onupdatefiles={(fileItems) => {
                                if (fileItems.length === 0) {
                                    console.log("onupdatefiles length 0 : ", fileItems)
                                }
                                console.log("onupdatefiles : ", fileItems)
                                setFiles(fileItems)
                            }}
                            allowRevert={false}
                            checkValidity={true}
                            acceptedFileTypes={['images/*']}
                            instantUpload={false}
                            fileValidateTypeDetectType={(source, type) => new Promise((resolve, reject) => {
                                console.log("Validation : ", type)
                                resolve(type);
                            })
                            }
                            allowMultiple={true}
                            maxFiles={1}
                            server={{
                                process: (fieldName, file, metadata, load) => {
                                    // simulates uploading a file
                                    console.log("server proses : ", fieldName, file, metadata)
                                },
                                load: (source, load) => {
                                    // simulates loading a file from the server
                                    console.log("Load : ", source)
                                    fetch(source)
                                        .then((res) => res.blob())
                                        .then(load);
                                },
                            }}
                            onprocessfile={(err, file) => {
                                if (err) {
                                    console.log("Error onprosessFile : ", err)
                                }
                                console.log('FilePond ready for use', err, file.filename);
                                console.log('test', file.serverId);
                            }}
                            name="files"   //{* sets the file input name, it's filepond by default */}
                            labelIdle='Drag & Drop your files or <span class="filepond--label-action">Browse</span>'
                        />
                        <Space h="xs" />
                        <TextInput label="Title" placeholder="title" name="title" data-autofocus onChange={(rst) => { onChangeTitle(rst.currentTarget.value) }} />
                        <Space h="xs" />
                        <Textarea label="Detail" placeholder="Detail" name="detail" autosize
                            minRows={2} maxRows={7} data-autofocus onChange={(rst) => { onChangeDetail(rst.currentTarget.value) }} />
                        <Text size="xs" style={{ marginBottom: 0 }} color="blue" weight={400}>
                            Gunakan format 1 baris dan gunakan tanda ( _b ) untuk baris baru.
                        </Text>
                        <Space h="xs" />
                        <Select name="brand" onChange={(rst) => { onChangeBrand(rst) }}
                            label="Brand"
                            placeholder="Pick one"
                            data={[
                                { value: 'gl', label: 'gl' },
                                { value: 'tea', label: 'tea' },
                                { value: 'ghg', label: 'ghg' },
                                { value: 'sozo', label: 'sozo' },
                            ]}
                        />
                        <Space h="sm" />
                        {/* <Group position="apart" spacing="sm" grow> */}
                        <div>
                            <Text size="sm" style={{ marginBottom: 2 }} weight={500}>
                                Periode Start
                            </Text>
                            <div className="rounded-md border p-2">
                                <DatePicker
                                    dateFormat="dd/MM/yyyy h:mm aa"
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    selected={periodeStart}
                                    onChange={(rst) => { onChangePeriodeStart(rst) }}
                                // customInput={<ExampleCustomInput />}
                                />
                            </div>
                        </div>
                        <Space h="xs" />
                        <div>
                            <Text size="sm" style={{ marginBottom: 2 }} weight={500}>
                                Periode End
                            </Text>
                            <div className="rounded-md border p-2">
                                <DatePicker
                                    dateFormat="dd/MM/yyyy h:mm aa"
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    selected={periodeEnd}
                                    onChange={(rst) => { onChangePeriodeEnd(rst) }}
                                // customInput={<ExampleCustomInput />}
                                />
                            </div>
                        </div>
                        {/* </Group> */}
                        <Space h="xs" />
                        <Group position="apart" spacing="sm" grow>
                            <NumberInput
                                defaultValue={0}
                                placeholder="0"
                                label="Max Redeem"
                                onChange={(rst) => { onChangeMaxRedeem(rst) }}
                            />
                            <NumberInput
                                defaultValue={1}
                                placeholder="1"
                                label="Short id"
                                onChange={(rst) => { onChangeShortId(rst) }}
                            />
                        </Group>
                        <Space h="xs" />
                        <InputWrapper
                            id="Status"
                            label="Status"
                            error="Please active your item"
                        >
                            <Switch name="status" label="ON / OFF" onLabel="ON" offLabel="OFF" onChange={(rst) => { onChangeStatus(rst.currentTarget.checked) }} />
                        </InputWrapper>
                        <Space h="xs" />
                        <Group position="right">
                            <Button variant="outlined" onClick={() => closeModal()} mt="md">
                                Cancel
                            </Button>
                            <Space w="xs" />
                            <Button variant="outlined" onClick={() => saveCoupon()} mt="md">
                                Save
                            </Button>
                        </Group>
                    </div>
                </Modal>

                {/* #MODAL EDIT COUPONS_MENU  */}
                <Modal
                    centered
                    opened={isOpenEdit}
                    onClose={() => closeModal()}
                    title={"COUPON ID : " + modelCoupons.idfs}
                    overflow="inside"
                >
                    <div>
                        <FilePond
                            ref={(ref) => { setFilePond(ref) }}
                            files={filesOption}
                            onupdatefiles={setFiles}
                            acceptedFileTypes={['images/*']}
                            instantUpload={false}
                            allowMultiple={false}
                            maxFiles={1}
                            server={{
                                process: (fieldName, file, metadata, load) => {
                                    // simulates uploading a file
                                    console.log("server proses : ", fieldName, file, metadata)
                                },
                                load: (source, load) => {
                                    // simulates loading a file from the server
                                    console.log("Load : ", source)
                                    fetch(source)
                                        .then((res) => res.blob())
                                        .then(load);
                                },
                            }}
                            onprocessfile={(err, file) => {
                                if (err) {
                                    console.log("Error onprosessFile : ", err)
                                }
                                console.log('FilePond ready for use', err, file.filename);
                                console.log('test', file.serverId);
                            }}
                            name="files"   //{* sets the file input name, it's filepond by default */}
                            labelIdle='Drag & Drop your files or <span class="filepond--label-action">Browse</span>'
                        />
                        <Space h="xs" />
                        <TextInput label="Title" placeholder="title" name="title" data-autofocus defaultValue={dataSelected.title} onChange={(rst) => { onChangeTitle(rst.currentTarget.value) }} />
                        <Space h="xs" />
                        <Textarea label="Detail" placeholder="Detail" name="detail" autosize
                            minRows={2} maxRows={7} data-autofocus defaultValue={dataSelected.detail} onChange={(rst) => { onChangeDetail(rst.currentTarget.value) }} />
                        <Text size="xs" style={{ marginBottom: 0 }} color="blue" weight={400}>
                            Gunakan format 1 baris dan gunakan tanda ( _b ) untuk baris baru.
                        </Text>
                        <Space h="xs" />
                        <Select name="brand" defaultValue={dataSelected.brand} onChange={(rst) => { onChangeBrand(rst) }}
                            label="Brand"
                            placeholder="Pick one"
                            data={[
                                { value: 'gl', label: 'gl' },
                                { value: 'tea', label: 'tea' },
                                { value: 'ghg', label: 'ghg' },
                                { value: 'sozo', label: 'sozo' },
                            ]}
                        />
                        <Space h="sm" />
                        {/* <Group position="apart" spacing="sm" grow> */}
                        <div>
                            <Text size="sm" style={{ marginBottom: 2 }} weight={500}>
                                Periode Start
                            </Text>
                            <div className="rounded-md border p-2 text-sm">
                                <DatePicker
                                    dateFormat="dd/MM/yyyy h:mm aa"
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    selected={new Date(modelCoupons.periode_start)}
                                    onChange={(rst) => { onChangePeriodeStart(rst) }}
                                // customInput={<ExampleCustomInput />}
                                />
                            </div>
                        </div>
                        <Space h="xs" />
                        <div>
                            <Text size="sm" style={{ marginBottom: 2 }} weight={500}>
                                Periode End
                            </Text>
                            <div className="rounded-md border p-2 text-sm ">
                                <DatePicker
                                    dateFormat="dd/MM/yyyy h:mm aa"
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    timeCaption="time"
                                    selected={new Date(modelCoupons.periode_end)}
                                    onChange={(rst) => { onChangePeriodeEnd(rst) }}
                                // customInput={<ExampleCustomInput />}
                                />
                            </div>
                        </div>
                        {/* </Group> */}
                        <Space h="xs" />
                        <Group position="apart" spacing="sm" grow>
                            <NumberInput
                                defaultValue={dataSelected.max_redem}
                                placeholder="0"
                                label="Max Redeem"
                                onChange={(rst) => { onChangeMaxRedeem(rst) }}
                            />
                            <NumberInput
                                defaultValue={dataSelected.id}
                                placeholder="1"
                                label="Short id"
                                onChange={(rst) => { onChangeShortId(rst) }}
                            />
                        </Group>
                        <Space h="xs" />
                        <InputWrapper
                            id="Status"
                            label="Status"
                            error="Please active your item"
                        >
                            <Switch name="status" label="ON / OFF" onLabel="ON" offLabel="OFF" onChange={(rst) => { onChangeStatus(rst.currentTarget.checked) }} />
                        </InputWrapper>
                        <Space h="xs" />
                        <Group position="right">
                            <Button variant="outlined" onClick={() => closeModal()} mt="md">
                                Cancel
                            </Button>
                            <Space w="xs" />
                            <Button variant="outlined" onClick={() => saveCouponEdit()} mt="md">
                                Save
                            </Button>
                        </Group>
                    </div>
                </Modal>

                {/* #MODAL DELETE COUPONS_MENU  */}
                <Modal
                    opened={isOpenDelete}
                    title="Konfirm Hapus"
                    onClose={() => setOpenedDelete(false)}
                >
                    <div>
                        <Space h="xs" />
                        <Text size="sm" style={{ marginBottom: 10 }} weight={500}>
                            Apakah anda yakin hapus kategori menu ini?
                        </Text>
                        <Group position="right">
                            <Button variant="outlined" onClick={() => setOpenedDelete(false)} mt="md">
                                Cancel
                            </Button>
                            <Space w="xs" />
                            <Button variant="outlined" onClick={() => deleteCoupon()} mt="md">
                                OK
                            </Button>
                        </Group>
                    </div>
                </Modal>

                {/* #MODAL CHANGE STATUS  */}
                <Modal
                    centered
                    opened={isOpenEditStatus}
                    onClose={() => closeModal()}
                    title={"Konfirm Update Data ID : " + modelCoupons.idfs}
                >
                    <div>
                        <Space h="xs" />
                        <Text size="sm" style={{ marginBottom: 10 }} weight={500}>
                            Apakah anda yakin update data ini?
                        </Text>
                        <Group position="right">
                            <Button variant="outlined" onClick={() => setOpenedStatus(false)} mt="md">
                                Cancel
                            </Button>
                            <Space w="xs" />
                            <Button variant="outlined" onClick={() => simpanDataEditStatus()} mt="md">
                                OK
                            </Button>
                        </Group>
                    </div>
                </Modal>

                <Sidebar active="coupons" />
                <div className='grow'>
                    <Header />
                    <Container fixed>
                        <Group position="right">
                            <Space h="md" />
                            <div className='grid grid-flow-col gap-1'>
                                <div>
                                    <Space h="md" />
                                    <Button variant="contained" href="#contained-buttons" onClick={() => { openAdd() }}>
                                        Add
                                    </Button>
                                </div>
                            </div>
                        </Group>
                    </Container>
                    <Container fixed>
                        <Space h="md" />
                        <div className='grid grid-flow-col gap-1'>
                            <div><MUIDataTable
                                title={"Coupons"}
                                data={dataCoupons}
                                columns={columns}
                                options={options}
                            /></div>
                        </div>
                    </Container>
                    <Container fixed>
                    </Container>
                </div>
            </div>
        )
    }
    return (
        <div className='flex justify-start space-x-0'>
            <Sidebar active="oupons" />
            <div className='grow'>
                <Header />
                <div className='p-5'>
                    content settings Coupons
                </div>
            </div>
        </div>
    )
}

export default CouponPage