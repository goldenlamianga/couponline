import axios from 'axios'
import Router from 'next/router'
import { useEffect, useState } from 'react'
import ReactLoading from "react-loading"
import Header from '../../components/admin/header'
import Sidebar from '../../components/admin/sidebar'
import cekAdminSession from '../../middleware/admin/cekAdminSession'
import userAdminLoginGet from '../../middleware/admin/userAdminLoginGet'

function AdminDashboard() {

    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [dataLoggedIn, setDataLoggedIn] = useState(false)
    const [loading, setLoading] = useState(false)

    const cekLogin = async () => {
        const rst = await userAdminLoginGet()
        console.log("cekLogin : " + rst)
        if (!rst) {
            setIsLoggedIn(true)
            Router.push('/admin/login')
        } else {
            setDataLoggedIn(rst)
        }
    }


    useEffect(() => {
        cekAdminSession()
        cekLogin()
    }, []);

    if (!isLoggedIn) {
        return (
            <div className='flex justify-start space-x-0'>
                <Sidebar active="dashboard" />
                <div className='grow'>
                    <Header />
                    <div className='p-5'>
                        <div>Welcome</div>
                    </div>

                </div>
            </div>
        )
    }
    return (
        <div className='flex justify-center items-center h-screen'>
            <ReactLoading type="bubbles" color="#4287f5" />
        </div>
    )
}

export default AdminDashboard