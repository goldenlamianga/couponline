import { useState, useEffect, React } from 'react'
import axios from 'axios'
import Router from 'next/router'
import userAdminLoginStore from '../../middleware/admin/userAdminLoginStore'
import decryptText from '../../middleware/encdecrypt/decryptText'
import { app, database } from '/firebaseConfig'
import { doc, collection, query, where, addDoc, getDoc, getDocs, orderBy, FieldValue } from 'firebase/firestore'

function LoginPage() {

    const [loading, setLoading] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPass] = useState("")
    const [msgStatus, setMsgStatus] = useState("")

    const login = async () => {
        setMsgStatus("")
        setLoading(true)
        var itemColl = []
        try {
            const dbInstance = collection(database, 'users-admin')
            const q = query(dbInstance, where("email", "==", email));
            const querySnapshot = await getDocs(q).then(async (data) => {
                setLoading(false)
                if (!data.empty) {
                    if (data.size == 1) {
                        data.docs.map((item, idx) => {
                            itemColl.push({ ...item.data(), id: item.id })
                        });
                        var cekPass = await decryptText(itemColl[0].password)
                        if (cekPass == password) {
                            userAdminLoginStore(itemColl[0])
                            Router.push("/admin")
                        } else {
                            setMsgStatus("Email/Password salah")
                        }
                    }
                } else {
                    setMsgStatus("Email tidak terdaftar.")
                }
            });
        } catch (error) {
            setLoading(false)
            setMsgStatus("Login Failed.")
            console.log("->error : " + error)
        }
    }

    function setParamUi() {
        const passwordToggle = document.querySelector('.js-password-toggle')

        passwordToggle.addEventListener('change', function () {
            const password = document.querySelector('.js-password'),
                passwordLabel = document.querySelector('.js-password-label')

            if (password.type === 'password') {
                password.type = 'text'
                passwordLabel.innerHTML = 'hide'
            } else {
                password.type = 'password'
                passwordLabel.innerHTML = 'show'
            }

            password.focus()
        })
    }

    useEffect(() => {
        setParamUi()
    }, []);

    return (
        <div className="bg-gray-200 h-screen text-base text-grey-darkest font-normal relative">
            <div className="container mx-auto p-8">
                <div className="mx-auto max-w-sm pt-10">
                    {/* <div className="bg-white rounded-t-md shadow mt-10 py-5 text-center flex justify-center">
                        <img src="/logo.png" layout='fill' alt="Logo" className="h-16" />
                    </div> */}

                    <div className="bg-white rounded-b-md shadow">
                        <div className="py-4 font-bold text-black text-center text-xl tracking-widest uppercase">
                            Golden lamian
                        </div>

                        <form className="p-4 max-w-md mx-auto bg-white border-t-8 border-red-700 rounded">
                            <h1 className="font-medium text-xl text-center py-2 text-gray-800">Coupons Admin</h1>
                            <label className="font-medium block mb-1 mt-6 text-gray-700" htmlFor="username">username
                            </label>
                            <input className="appearance-none border-2 rounded w-full py-3 px-3 leading-tight border-gray-300 bg-gray-100 focus:outline-none focus:border-red-700 focus:bg-white text-gray-700 pr-16 font-mono" id="username" type="text" autoComplete="off" autoFocus placeholder='Email' onChange={(e) => setEmail(e.target.value)} />

                            <label className="font-medium block mb-1 mt-6 text-gray-700" htmlFor="password">password
                            </label>
                            <div className="relative w-full">
                                <div className="absolute inset-y-0 right-0 flex items-center px-2">
                                    <input className="hidden js-password-toggle" id="toggle" type="checkbox" />
                                    <label className="bg-gray-300 hover:bg-gray-400 rounded px-2 py-1 text-sm text-gray-600 font-mono cursor-pointer js-password-label" htmlFor="toggle">show</label>
                                </div>
                                <input className="appearance-none border-2 rounded w-full py-3 px-3 leading-tight border-gray-300 bg-gray-100 focus:outline-none focus:border-red-700 focus:bg-white text-gray-700 pr-16 font-mono js-password" id="password" type="password" autoComplete="off" placeholder='password' onChange={(e) => setPass(e.target.value)} />
                            </div>

                            <div className='text-xs text-center mt-3 text-seven-red '>{msgStatus}</div>

                            <button className="w-full bg-red-700 hover:bg-red-900 text-white font-medium py-3 px-4 mt-5 rounded focus:outline-none focus:shadow-outline" type="button" onClick={() => login()}>
                                {loading ? "Loading..." : "Sign in"}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginPage