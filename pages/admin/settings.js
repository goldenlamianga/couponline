import React from 'react'
import Header from '../../components/admin/header'
import Sidebar from '../../components/admin/sidebar'

function AdminSetting() {
    return (
        <div className='flex justify-start space-x-0'>
            <Sidebar active="settings" />
            <div className='grow'>
                <Header />
                <div className='p-5'>
                    User pengguna settings
                </div>
            </div>
        </div>
    )
}

export default AdminSetting